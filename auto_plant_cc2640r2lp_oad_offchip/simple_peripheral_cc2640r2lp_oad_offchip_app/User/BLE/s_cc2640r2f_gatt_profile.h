/*
 * s_cc2640r2f_gatt_profile.h
 *
 *  Created on: Sep 25, 2019
 *      Author: hemingway
 */

#ifndef USER_BLE_S_CC2640R2F_GATT_PROFILE_H_
#define USER_BLE_S_CC2640R2F_GATT_PROFILE_H_

/* This Header file contains all BLE API and icall structure definition */
#include "icall_ble_api.h"

/*
 *  The UUID length(16bit/128bit), in real test, 128bit is more stable
 *  16bit : ATT_BT_UUID_SIZE
 *  128bit : ATT_UUID_SIZE
 * */
#define GATT_UUID_LEN                               ATT_UUID_SIZE

//Parament ID
#define GATT_SERVICE1_CHAR1                         0

#define GATT_SERVICE1_CHAR_NUM						2

#define GATT_SERVICE1_UUID                          0xFFF0
#define GATT_SERVICE1_CHAR1UUID                     0xFFF1
// #define GATT_SERVICE1_CHAR2UUID						0xFFF2

#define GATT_SERVICE1_CHAR1DATALEN                  30
#define GATT_SERVICE1_CHAR2DATALEN					5

//Gatt profile service bit
#define GATT_PROFILE_SERVICE                        0x00000001

/*
 * Use a period clock to notify or notify once event happened
 * 0 : Don't use period clock
 * 1 : Use period clock
 * */
#define GATT_SERVICE1_PERIOD_NOTIFY                 0

/*
 *  Gatt profile callback
 * */
typedef void (*pGattServiceProfileChange_t)(unsigned char paramID);

typedef struct
{
    pGattServiceProfileChange_t        pfnGattProfileChange;  // Called when characteristic value changes
} gattProfileCallback_t;

extern unsigned char gattService1Char1Data[GATT_SERVICE1_CHAR1DATALEN];
extern volatile unsigned short gattService1Char1Datalen;

extern unsigned char gattService1Char2Data[GATT_SERVICE1_CHAR2DATALEN];
extern volatile unsigned short gattService1Char2Datalen;

extern bStatus_t S_CC2640R2F_Gatt_Profile_AddService(unsigned int service);
extern bStatus_t gattProfileRegisterAppCallback(gattProfileCallback_t *gattCallback);
extern bStatus_t S_CC2640R2F_Gatt_Profile_NotifyOutData(unsigned char paraID, \
                                                        unsigned char *data, \
                                                        unsigned char length);
extern bStatus_t S_CC2640R2F_Gatt_Profile_Handle_BLE_ReceivedData(unsigned char paramID);

#endif /* USER_BLE_S_CC2640R2F_GATT_PROFILE_H_ */
