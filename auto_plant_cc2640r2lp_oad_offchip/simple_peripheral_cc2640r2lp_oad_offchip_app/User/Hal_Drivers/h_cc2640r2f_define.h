/*
 * h_cc2640r2f_define.h
 *
 *  Created on: Sep 2, 2020
 *      Author: hemingway
 */

#ifndef USER_HAL_DRIVERS_H_CC2640R2F_DEFINE_H_
#define USER_HAL_DRIVERS_H_CC2640R2F_DEFINE_H_

#include <ti/drivers/PIN.h>
#include <ti/devices/cc26x0r2/driverlib/ioc.h>

/*
 * 1 for not use
 * 0 for use
 * */
#define DO_NOT_USE_UNIQUE_GAPROLE_ADVERDATA     1

/*
 * 1 for not use
 * 0 for use
 * */
#define DO_NOT_USE_UNIQUE_GAPROLE_SCANRSPDATA   1

/*
 * 0 for not use
 * 1 for use
 * */
#define ENABLE_GAPROLE_BDADDR                   0

/*
 * 0 for not use
 * 1 for use
 * */
#define ENABLE_GAPROLE_ADVDIRECTADDR            1

#if DO_NOT_USE_UNIQUE_GAPROLE_ADVERDATA
#define GAPROLE_ADVERDATA_LEN                   9
#endif

#if DO_NOT_USE_UNIQUE_GAPROLE_SCANRSPDATA
#define GAPROLE_SCANRSPDATA_LEN                 24
#endif
/*
 * The ble device information list
 * 0 for don't show the device information
 * 1 for show the device information
 * */
#define DEVINFO_ENABLE                  0
/*
 * 0 for disable ble gatt bond manager
 * 1 for enable ble gatt bond manager
 * */
#define BLE_BOND_ENABLE                 0

///*
// * 0 for disable ble gatt access information
// * 1 for enable it
// * */
#define BLE_GATT_ACCESS                 0
//
///*
// * 0 for disable ble gatt attribute information
// * 1 for enable it
// * */
#define BLE_GATT_ATTRIBUTE              0

#define AUTO_PLANT_TASK_SIZE                    1024//1536//1024
#define AUTO_PLANT_TASK_PRIORITY                2


#define SIMPLE_AROUND_LOOP_PERIOD               10000//4300//12000

#define H_CC2640R2F_USE_MALLOC                  1

#define LOCK_PWM_HARDWARE_MODE					1

#define USE_INNER_RTC							1

#if H_CC2640R2F_USE_MALLOC
#include <icall_ble_api.h>
#define H_CC2640R2F_DEBUG_MALLOC                ICall_malloc
#define H_CC2640R2F_DEBUG_FREE                  ICall_free
#endif

#define H_CC2640R2F_DEBUG_BaudRate              115200
#define CSAM_RX_MAX_LENGTH                      16

/*
 * Special function IO
 * */
#define AUTO_PLANT_LED_ON                       0
#define AUTO_PLANT_LED_OFF                      1

#define AUTO_PLANT_BAT_ADC_IO                   IOID_22     //Channel0
#define AUTO_PLANT_DATA0_ADC_IO                 IOID_28     //Channel1
#define AUTO_PLANT_DATA1_ADC_IO                 IOID_29     //Channel2

#define AUTO_PLANT_BUZZER_PWM_IO                IOID_26     //Channel0
#define AUTO_PLANT_A_PWM_IO                     IOID_14     //Channel1
#define AUTO_PLANT_B_PWM_IO                     IOID_13     //Channel2

#define AUTO_PLANT_LED_GREEN_IO                 IOID_24
#define AUTO_PLANT_NOR_FLASH_CS_IO              IOID_12
#define AUTO_PLANT_UART_TX_IO                   IOID_23

#define AUTO_PLANT_PERIPHERAL_ENABLE_IO			IOID_22

#define AUTO_PLANT_SD3077_INT_IO				IOID_0


/*
 * Re define
 * */
// #undef CC2640R2_LAUNCHXL_DIO23_ANALOG
// #define CC2640R2_LAUNCHXL_DIO23_ANALOG          AUTO_PLANT_BAT_ADC_IO
// #undef CC2640R2_LAUNCHXL_DIO24_ANALOG
// #define CC2640R2_LAUNCHXL_DIO24_ANALOG          AUTO_PLANT_DATA0_ADC_IO
// #undef CC2640R2_LAUNCHXL_DIO25_ANALOG
// #define CC2640R2_LAUNCHXL_DIO25_ANALOG          AUTO_PLANT_DATA1_ADC_IO

#undef CC2640R2_LAUNCHXL_PWMPIN0
#define CC2640R2_LAUNCHXL_PWMPIN0               AUTO_PLANT_BUZZER_PWM_IO
#undef CC2640R2_LAUNCHXL_PWMPIN1
#define CC2640R2_LAUNCHXL_PWMPIN1               AUTO_PLANT_A_PWM_IO
#undef CC2640R2_LAUNCHXL_PWMPIN2
#define CC2640R2_LAUNCHXL_PWMPIN2               AUTO_PLANT_B_PWM_IO

//#undef CC2640R2_LAUNCHXL_DIO24_ANALOG
//#define CC2640R2_LAUNCHXL_DIO24_ANALOG          PIN_UNASSIGNED
//#undef CC2640R2_LAUNCHXL_DIO28_ANALOG
//#define CC2640R2_LAUNCHXL_DIO28_ANALOG          PIN_UNASSIGNED
//#undef CC2640R2_LAUNCHXL_DIO29_ANALOG
//#define CC2640R2_LAUNCHXL_DIO29_ANALOG          PIN_UNASSIGNED
//#undef CC2640R2_LAUNCHXL_LCD_CS
//#define CC2640R2_LAUNCHXL_LCD_CS                PIN_UNASSIGNED


#undef CC2640R2_LAUNCHXL_PIN_LED_ON
#define CC2640R2_LAUNCHXL_PIN_LED_ON            AUTO_PLANT_LED_ON
#undef CC2640R2_LAUNCHXL_PIN_LED_OFF
#define CC2640R2_LAUNCHXL_PIN_LED_OFF           AUTO_PLANT_LED_OFF
#undef CC2640R2_LAUNCHXL_PIN_GLED
#define CC2640R2_LAUNCHXL_PIN_GLED              AUTO_PLANT_LED_GREEN_IO
#undef CC2640R2_LAUNCHXL_SPI_FLASH_CS
#define CC2640R2_LAUNCHXL_SPI_FLASH_CS          AUTO_PLANT_NOR_FLASH_CS_IO
#undef CC2640R2_LAUNCHXL_UART_TX
#define CC2640R2_LAUNCHXL_UART_TX               AUTO_PLANT_UART_TX_IO

extern void debug_hex(unsigned char *data, unsigned short len);

#endif /* USER_HAL_DRIVERS_H_CC2640R2F_DEFINE_H_ */
