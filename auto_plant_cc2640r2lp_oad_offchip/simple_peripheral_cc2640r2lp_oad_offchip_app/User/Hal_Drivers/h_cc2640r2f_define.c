/*
 * h_cc2640r2f_define.c
 *
 *  Created on: Sep 2, 2020
 *      Author: hemingway
 */
#include "h_cc2640r2f_define.h"

#include "h_cc2640r2f_uart.h"

#include <stdio.h>
//#include <stdarg.h>
#include <string.h>

#define H_CC2640R2F_DEBUG_TX        H_CC2640R2F_UART_TxBuff

int fputc(int _c, register FILE *_fp)
{
    H_CC2640R2F_DEBUG_TX((const unsigned char *)&_c, 1);
    return(_c);
}

int fputs(const char *_ptr, register FILE *_fp)
{
    unsigned short len = strlen(_ptr);
    H_CC2640R2F_DEBUG_TX((const unsigned char *)_ptr, len);

    return len;
}

#define STR2HEX_LEN     128
void str2hex(unsigned char *pAddr, unsigned short len, unsigned char *str2hexdata)
{
  unsigned short     charCnt;
  const unsigned char        hex[] = "0123456789ABCDEF";
  unsigned char        *pStr = str2hexdata;

  for (charCnt = 0; charCnt < len; charCnt++)
  {
    *pStr++ = hex[*pAddr >> 4];
    *pStr++ = hex[*pAddr++ & 0x0F];
  }

  *pStr = '\0';

  pStr = NULL;
}



void debug_hex(unsigned char *data, unsigned short len)
{
  unsigned short i = 0;
  unsigned short d_len = 0;

  unsigned char *tdata = data;

  unsigned char str2hexdata[STR2HEX_LEN + STR2HEX_LEN + 1];

  for(i = 0; i <= len; i += STR2HEX_LEN)
  {
    d_len = len < STR2HEX_LEN ? len : STR2HEX_LEN;

    str2hex(tdata, d_len, &str2hexdata[0]);

    printf("%s", &str2hexdata[0]);

    if(len < STR2HEX_LEN)
    {
      break;
    }

    len -= STR2HEX_LEN;
    tdata += STR2HEX_LEN;
  }


  printf("\r\n");
}

