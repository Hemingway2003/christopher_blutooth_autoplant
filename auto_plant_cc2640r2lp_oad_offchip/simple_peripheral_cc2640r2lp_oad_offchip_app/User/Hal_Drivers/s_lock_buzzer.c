#include "s_lock_buzzer.h"

////////////////////////////////////////////////////////////////////////////////
#include "h_cc2640r2f_define.h"
#include "h_cc2640r2f_simpletime.h"


#include "h_cc2640r2f_pwm_simple.h"


#include <stdio.h>


#define s_lock_buzzer_delay                     H_CC2640R2F_SimpleTime_Around_Delay



static void s_lock_buzzer_pwm_start(void)
{
    H_CC2640R2F_PWM_StartChannel_ByIndex(0);
}

static void s_lock_buzzer_pwm_stop(void)
{
    H_CC2640R2F_PWM_StopChannel_ByIndex(0);
}


static void s_lock_buzzer_pwm_change_frq(unsigned int frq)
{
    H_CC2640R2F_PWM_ChannelSet_Per_ByIndex(0, frq);
}

signed char S_Lock_Buzzer_Init(void)
{
//    return H_CC2640R2F_PWM_Init_ByNum(0, 1024);
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

signed char S_Lock_Buzzer_Success(void)
{
    s_lock_buzzer_pwm_start();

    s_lock_buzzer_pwm_change_frq(294);
    s_lock_buzzer_delay(160);

    s_lock_buzzer_pwm_change_frq(589);
    s_lock_buzzer_delay(83);

    s_lock_buzzer_pwm_change_frq(661);
    s_lock_buzzer_delay(83);

    s_lock_buzzer_pwm_change_frq(700);
    s_lock_buzzer_delay(83);

    s_lock_buzzer_pwm_change_frq(786);
    s_lock_buzzer_delay(83);
    s_lock_buzzer_pwm_stop();
    return 0;
}

signed char S_Lock_Buzzer_Success_2(void)
{
    s_lock_buzzer_pwm_start();

    s_lock_buzzer_pwm_change_frq(262);
    s_lock_buzzer_delay(160);

    s_lock_buzzer_pwm_change_frq(589);
    s_lock_buzzer_delay(83);

    s_lock_buzzer_pwm_change_frq(661);
    s_lock_buzzer_delay(83);

    s_lock_buzzer_pwm_change_frq(700);
    s_lock_buzzer_delay(83);

    s_lock_buzzer_pwm_change_frq(786);
    s_lock_buzzer_delay(300);
    s_lock_buzzer_pwm_stop();
    return 0;
}

signed char S_Lock_Buzzer_LowPower(void)
{
    s_lock_buzzer_pwm_start();

    s_lock_buzzer_pwm_change_frq(880);
    s_lock_buzzer_delay(160);

    s_lock_buzzer_pwm_change_frq(786);
    s_lock_buzzer_delay(160);

    s_lock_buzzer_pwm_change_frq(700);
    s_lock_buzzer_delay(160);

    s_lock_buzzer_pwm_change_frq(661);
    s_lock_buzzer_delay(160);

    s_lock_buzzer_pwm_change_frq(589);
    s_lock_buzzer_delay(320);

    s_lock_buzzer_pwm_stop();
    return 0;
}

signed char S_Lock_Buzzer_Failed(void)
{
    s_lock_buzzer_pwm_start();

    s_lock_buzzer_pwm_change_frq(294);
    s_lock_buzzer_delay(300);

    s_lock_buzzer_pwm_stop();
    s_lock_buzzer_delay(83);
    s_lock_buzzer_pwm_start();

    s_lock_buzzer_pwm_change_frq(294);
    s_lock_buzzer_delay(300);

    s_lock_buzzer_pwm_stop();
    return 0;
}


//signed char S_Lock_Buzzer_KeyDown(void)
//{
//    s_lock_buzzer_pwm_start();
//
//    s_lock_buzzer_pwm_change_frq(589);
//    s_lock_buzzer_delay(50);
//
//    s_lock_buzzer_pwm_stop();
//    return 0;
//}




void S_Lock_Buzzer_Test(void)
{
    S_Lock_Buzzer_Success();

    s_lock_buzzer_delay(1000);

    S_Lock_Buzzer_Failed();
}
