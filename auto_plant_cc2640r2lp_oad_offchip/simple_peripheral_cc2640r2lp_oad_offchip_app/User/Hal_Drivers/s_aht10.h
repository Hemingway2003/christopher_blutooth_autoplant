/*
 * s_aht10.h
 *
 *  Created on: Sep 24, 2020
 *      Author: hemingway
 */

#ifndef USER_HAL_DRIVERS_S_AHT10_H_
#define USER_HAL_DRIVERS_S_AHT10_H_

extern void S_AHT10_Init(void);
extern signed char S_AHT10_Reset(void);
extern signed char S_AHT10_Start_Messure(void);
extern signed char S_AHT10_Get_Humidity_Temp(signed int *hum, signed int *temp);

#endif /* USER_HAL_DRIVERS_S_AHT10_H_ */
