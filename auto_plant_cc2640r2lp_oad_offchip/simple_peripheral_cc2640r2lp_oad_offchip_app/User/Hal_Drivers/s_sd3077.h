#ifndef _S_SD3077_H_
#define _S_SD3077_H_

#include "s_utc_2_normaltime.h"

typedef enum
{
    sd3077_clock_alarm_all_disable = 0,
    sd3077_clock_alarm_only_enable_seconde = 1,
    sd3077_clock_alarm_only_enable_seconde_min = 3,
    sd3077_clock_alarm_only_enable_seconde_min_hour = 7,
}sd3077_clock_alarm_mode;

typedef struct
{
  unsigned char hour;
  unsigned char min;
  unsigned char sec;
}SD3077_HMS_Data;

#define SD3077_IMPOSSIBLE_TIME						(86400 - 1)

extern void S_SD3077_Test(void);

extern signed char S_SD3077_GetDateStr(p_s_normaltime_str nmt);
extern signed char S_SD3077_GetDate(unsigned int *utc);
extern signed char S_SD3077_SetDate(unsigned int utc);

// extern signed char S_SD3077_SetInt_Hour_Min_Second(unsigned char hour, unsigned char min, unsigned char sec);
extern signed char S_SD3077_Set_ClockAlarmMode(sd3077_clock_alarm_mode mode);
extern signed char S_SD3077_Clear_IntrFlag(void);

extern signed char  S_SD3077_Reset_RTC_Intr(unsigned int up_sec);

extern void S_SD3077_Seconds_2_HMS(unsigned int sec, SD3077_HMS_Data *hms);

extern signed char S_SD3077_SetInt_Hour_Min_Second_ByDaySeconds(unsigned int secs);

extern unsigned int S_SD3077_Get_Time_Now_Second(void);

#endif
