#ifndef _S_LOCK_BUZZER_H_
#define _S_LOCK_BUZZER_H_

typedef enum
{
    Buzzer_Success = 0,
    Buzzer_Failed
}bruzzer_status;

extern signed char S_Lock_Buzzer_Failed(void);
extern signed char S_Lock_Buzzer_Success(void);
extern signed char S_Lock_Buzzer_Success_2(void);
extern signed char S_Lock_Buzzer_LowPower(void);
//extern signed char S_Lock_Buzzer_KeyDown(void);

extern signed char S_Lock_Buzzer_Init(void);

extern void S_Lock_Buzzer_Test(void);

#endif
