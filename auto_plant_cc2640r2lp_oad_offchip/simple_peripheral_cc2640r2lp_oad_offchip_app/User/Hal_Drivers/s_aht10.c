/*
 * s_aht10.c
 *
 *  Created on: Sep 24, 2020
 *      Author: hemingway
 */
#include "s_aht10.h"

#include "h_cc2640r2f_iic.h"
#include "h_cc2640r2f_simpletime.h"

#include <stdio.h>
#include "h_cc2640r2f_define.h"
#include "s_auto_plant_para.h"

#define S_AHT10_IIC_ADDR                    0x38//0x64//0x32

#define S_AHT10_STATE_REG                   0x71
#define S_AHT10_READY_BIT                   (1 << 3)
#define S_AHT10_BUSY_BIT                    (1 << 7)

#define S_AHT10_RESET_REG                   0xE1
#define S_AHT10_START_MESSURE_REG           0xAC

#define s_aht10_read(x,y,z)                 H_CC2640R2F_IIC_ReadBuffer(S_AHT10_IIC_ADDR,x,y,z)
#define s_aht10_write(x,y,z)                H_CC2640R2F_IIC_WriteBuffer(S_AHT10_IIC_ADDR,x,y,z)

static const unsigned char S_AHT10_RESET_CMD[] = {0x08, 0x00};
static const unsigned char S_AHT10_START_MESSURE_CMD[] = {0x33, 0x00};

static unsigned char s_aht10_get_state(void)
{
    unsigned char state[1] = {0};

    s_aht10_read(S_AHT10_STATE_REG, state, 1);

    return state[0];
}

static signed char s_aht10_check_enable(void)
{
    if(s_aht10_get_state() & S_AHT10_READY_BIT != S_AHT10_READY_BIT)
    {
        // Not enable
        // printf("not enbale\n");
        return 1;
    }
    else
    {
        return 0;
    }
}

static signed char s_aht10_check_busybit(void)
{
    if(s_aht10_get_state() & S_AHT10_BUSY_BIT != S_AHT10_BUSY_BIT)
    {
        // Not enable
        // printf("busy\n");
        return 1;
    }
    else
    {
        return 0;
    }
}

signed char S_AHT10_Reset(void)
{
    if(s_aht10_check_enable()) {
        S_AHT10_Reset();
        return (-1);
    } else {
        return s_aht10_write(S_AHT10_RESET_REG, S_AHT10_RESET_CMD, sizeof(S_AHT10_RESET_CMD));
    }
}

signed char S_AHT10_Start_Messure(void)
{
    return s_aht10_write(S_AHT10_START_MESSURE_REG, S_AHT10_START_MESSURE_CMD, sizeof(S_AHT10_START_MESSURE_CMD));
}

signed char S_AHT10_Get_Humidity_Temp(signed int *hum, signed int *temp)
{
    unsigned char rdata[6];
    if(0 == s_aht10_check_busybit())
    {
        s_aht10_read(S_AHT10_STATE_REG, rdata, 6);
        // debug_hex(rdata, 6);

        *hum = rdata[1] << 16 | rdata[2] << 8 | rdata[3];
        *hum >>= 4;
        *hum *= 100;
        *hum /= 1048576; //1024 * 1024;
        // printf("the hum is %d\r\n", *hum);

        *temp = ((rdata[3] & 0x0F) << 16) | rdata[4] << 8 | rdata[5];
        *temp *= 200;
        *temp = (*temp / 1048576) - 50;
        // printf("the temp is %d\r\n", *temp);
        return 0;
    }
    else
    {
        *hum = 0;
        *temp = 0;
        S_AHT10_Reset();
        return 1;
    }

}

void S_AHT10_Init(void)
{
//  signed int hum, temp;

    H_CC2640R2F_SimpleTime_Around_Delay(200);
    S_AHT10_Reset();
    // printf("the reset is %d\r\n", S_AHT10_Reset());

    // printf("the state is 0x%02x\r\n", s_aht10_get_state());
    // printf("the enable is 0x%02\r\n\n", s_aht10_check_enable());

    // S_AHT10_Start_Messure();
    // H_CC2640R2F_SimpleTime_Around_Delay(200);
    // S_AHT10_Get_Humidity_Temp(&hum, &temp);
}
