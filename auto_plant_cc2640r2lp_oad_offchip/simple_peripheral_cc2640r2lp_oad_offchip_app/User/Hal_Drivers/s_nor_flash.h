/*
 * s_nor_flash.h
 *
 *  Created on: Sep 11, 2020
 *      Author: hemingway
 */

#ifndef USER_HAL_DRIVERS_S_NOR_FLASH_H_
#define USER_HAL_DRIVERS_S_NOR_FLASH_H_

extern signed char S_Nor_Flash_Init(void);

extern signed char S_Nor_Flash_Test(void);

extern signed char S_Nor_Flash_Write_Para_Struct(unsigned char *data, unsigned short len);
extern signed char S_Nor_Flash_Read_Para_Struct(unsigned char *data, unsigned short len);

#endif /* USER_HAL_DRIVERS_S_NOR_FLASH_H_ */
