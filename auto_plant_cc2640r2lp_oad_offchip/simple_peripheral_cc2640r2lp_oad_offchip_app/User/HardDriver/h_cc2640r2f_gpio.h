/*
 * h_cc2640r2f_gpio.h
 *
 *  Created on: Sep 3, 2020
 *      Author: hemingway
 */

#ifndef USER_HARDDRIVER_H_CC2640R2F_GPIO_H_
#define USER_HARDDRIVER_H_CC2640R2F_GPIO_H_

#include "CC2640R2_LAUNCHXL.h"
#include <ti/drivers/PIN.h>
#include <ti/devices/cc26x0r2/driverlib/ioc.h>

extern signed char H_CC2640R2F_GPIO_Init(void);
extern signed char H_CC2640R2F_GPIO_OutputSet(unsigned char PinID, unsigned int value);
// extern signed char H_CC2640R2F_GPIO_OutputToggle(unsigned char PinID);

#endif /* USER_HARDDRIVER_H_CC2640R2F_GPIO_H_ */
