#ifndef _H_CC2640R2F_PWM_SIMPLE_H_
#define _H_CC2640R2F_PWM_SIMPLE_H_

#include "h_cc2640r2f_define.h"

#if LOCK_PWM_HARDWARE_MODE
// extern void H_CC2640R2F_PWM0_Test(void);

// extern signed char H_CC2640R2F_PWM0_ChannelSet_Per_Duty(unsigned short per, unsigned short duty);
// extern void H_CC2640R2F_PWM0_StartChannel(void);
// extern void H_CC2640R2F_PWM0_StopChannel(void);
// extern signed char H_CC2640R2F_PWM0_ChannelSet_Per(unsigned short per);
// extern signed char H_CC2640R2F_PWM0_Init(unsigned short freq);
//extern signed char H_CC2640R2F_PWM_Init_ByNum(unsigned char pwm_num, unsigned short freq);
//extern void H_CC2640R2F_PWM_StartChannel(void);
//extern void H_CC2640R2F_PWM_StopChannel(void);
//extern signed char H_CC2640R2F_PWM_ChannelSet_Per(unsigned short per);
//extern signed char H_CC2640R2F_PWM_ChannelSet_Duty_Persent(unsigned short duty);
extern signed char H_CC2640R2F_PWM_Init_MaxNums(unsigned char pwm_max_nums, unsigned short basic_freq);
extern signed char H_CC2640R2F_PWM_ChannelSet_Per_ByIndex(unsigned char index, unsigned short per);
extern signed char H_CC2640R2F_PWM_StartChannel_ByIndex(unsigned char index);
extern signed char H_CC2640R2F_PWM_StopChannel_ByIndex(unsigned char index);
extern signed char H_CC2640R2F_PWM_SetDuty_ByIndex(unsigned char index, unsigned int persent);
#else
// Softmod

extern signed char H_CC2640R2F_PWM_Init_ByNum(unsigned char pwm_num, unsigned short freq);
extern void H_CC2640R2F_PWM_StartChannel(void);
extern void H_CC2640R2F_PWM_StopChannel(void);
extern signed char H_CC2640R2F_PWM_ChannelSet_Per(unsigned short per);
extern signed char H_CC2640R2F_PWM_ChannelSet_Duty_Persent(unsigned short duty);
#endif

#endif
