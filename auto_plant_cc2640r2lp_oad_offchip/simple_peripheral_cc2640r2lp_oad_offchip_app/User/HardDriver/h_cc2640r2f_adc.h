/*
 * h_cc2640r2f_adc.h
 *
 *  Created on: Sep 3, 2020
 *      Author: hemingway
 */

#ifndef USER_HARDDRIVER_H_CC2640R2F_ADC_H_
#define USER_HARDDRIVER_H_CC2640R2F_ADC_H_

extern signed int H_CC2640R2F_ADC_Read(void);
// extern unsigned int H_CC2640R2F_ADC_Read_MicroVolts(void);
extern unsigned int H_CC2640R2F_ADC_Read_MicroVolts_byNum(unsigned char adc_num);
extern signed char H_CC2640R2F_ADC_Init(void);

#endif /* USER_HARDDRIVER_H_CC2640R2F_ADC_H_ */
