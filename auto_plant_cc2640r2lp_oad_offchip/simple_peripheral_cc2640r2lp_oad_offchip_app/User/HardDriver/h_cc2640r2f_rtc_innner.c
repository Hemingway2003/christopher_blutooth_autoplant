/*
 * h_cc2640r2f_rtc_innner.c
 *
 *  Created on: Sep 24, 2020
 *      Author: hemingway
 */
#include "h_cc2640r2f_rtc_innner.h"
#include <stdio.h>
#include <string.h>
#include <driverlib/aon_rtc.h>
#include <driverlib/aon_event.h>
#include "h_cc2640r2f_simpletime.h"

#define INNER_RTC_MAX_RUN_SEC           3600U

//signed char h_cc2640r2f_rtc_inner_set(unsigned int sec)
//{
//    HWREG( AON_RTC_BASE + AON_RTC_O_SEC) = sec;
//    return 0;
//}

//void AONRTCIntHandler(void)
//{
//    if (AONRTCEventGet(AON_RTC_CH0))
//    {
//        AONRTCEventClear(AON_RTC_CH0);
////        GPIO_toggleDio(6);
//        printf("Get\r\n");
//    }
//}

/*
 *  channel 0 is used by TI-RTOS, channel 2 is used by sensor controller applications
 * */
void __attribute__((weak)) timer_hookCH1_Handle(void);
void timer_hookCH1_Handle(void)
{
    printf("Test\r\n");
}

signed char h_cc2640r2f_rtc_inner_ch1_init(void)
{
    AONRTCEventClear(AON_RTC_CH1);
    AONRTCChannelEnable(AON_RTC_CH1);
    AONRTCCombinedEventConfig(AON_RTC_CH0 | AON_RTC_CH1);
    return 0;
}

signed char h_cc2640r2f_rtc_inner_ch1_setValueSec(unsigned int sec)
{
    unsigned int rtc_now = AONRTCCurrentCompareValueGet();
    // if(sec >= 65535U) {
    //     sec = 65535U;
    // }
    if(sec >= INNER_RTC_MAX_RUN_SEC) {
        sec = INNER_RTC_MAX_RUN_SEC;
    }
    AONRTCCompareValueSet(AON_RTC_CH1, (sec << 16) + rtc_now);
    return 0;
}

signed char h_cc2640r2f_rtc_inner_test(void)
{
    h_cc2640r2f_rtc_inner_ch1_init();
    h_cc2640r2f_rtc_inner_ch1_setValueSec(5);
    return 0;
}
