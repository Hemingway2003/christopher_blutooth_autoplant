/*
 * h_cc2640r2f_uart.h
 *
 *  Created on: Sep 19, 2019
 *      Author: hemingway
 */

#ifndef USER_HARDDRIVER_H_CC2640R2F_UART_H_
#define USER_HARDDRIVER_H_CC2640R2F_UART_H_

typedef enum
{
    H_CC2640R2F_UART_OK = 0,
    H_CC2640R2F_UART_Open_Failed,
    H_CC2640R2F_UART_Sema_Failed,
    H_CC2640R2F_UART_malloc_Failed,
}h_cc2640r2f_uart_rslt;

extern unsigned char *H_CC2640R2F_UARTRxBuffer;
extern volatile unsigned short H_CC2640R2F_UARTRxBufferLength;

extern h_cc2640r2f_uart_rslt H_CC2640R2F_UART_Init(void);
extern void H_CC2640R2F_UART_Deinit(void);

extern void H_CC2640R2F_UART_TxBuff(const unsigned char *data, \
                                    unsigned short length);
extern void H_CC2640R2F_UART_RxEnable(void);

extern h_cc2640r2f_uart_rslt H_CC2640R2F_UART_Rx_Data_Init(void);
extern void H_CC2640R2F_UART_Rx_Data_Release(void);

extern void H_CC2640R2F_UART_RxDisable(void);

#endif /* USER_HARDDRIVER_H_CC2640R2F_UART_H_ */

///*
// * h_cc2640r2f_uart.h
// *
// *  Created on: Sep 19, 2019
// *      Author: hemingway
// */
//
//#ifndef USER_HARDDRIVER_H_CC2640R2F_UART_H_
//#define USER_HARDDRIVER_H_CC2640R2F_UART_H_
//
//extern unsigned char H_CC2640R2F_UARTRxBuffer[];
//extern volatile unsigned short H_CC2640R2F_UARTRxBufferLength;
//
//extern signed char H_CC2640R2F_UART_Init(void);
//
//extern void H_CC2640R2F_UART_TxBuff(const unsigned char *data, \
//                                    unsigned short length);
//extern void H_CC2640R2F_UART_RxEnable(void);
//
////extern void H_CC2640R2F_UART_printf(const char* format, ...);
//
//
//#endif /* USER_HARDDRIVER_H_CC2640R2F_UART_H_ */
