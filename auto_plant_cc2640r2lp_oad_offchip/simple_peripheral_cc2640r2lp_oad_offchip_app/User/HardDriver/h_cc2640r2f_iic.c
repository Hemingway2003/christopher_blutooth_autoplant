#include "h_cc2640r2f_iic.h"

#include <ti/drivers/I2C.h>
#include "h_cc2640r2f_define.h"

#include "h_cc2640r2f_uart.h"

#include <icall_ble_api.h>
////////////////////////////////////////////////////////////////////////////////

#define H_CC2640R2F_IIC_BaudRate                I2C_100kHz

////////////////////////////////////////////////////////////////////////////////

static I2C_Handle H_CC2640R2F_I2CHandle = NULL;

/*
static void h_cc2640r2f_iic_callbackFxn(I2C_Handle handle, \
                                            I2C_Transaction *transaction, \
                                              bool transferStatus)
{
    H_CC2640R2F_UART_Debug("IIC callbck is %d\r\n", transferStatus);
}
*/

signed char H_CC2640R2F_IIC_Init(void)
{
    I2C_Params H_CC2640R2F_I2Cparams;
    I2C_init();
    
    I2C_Params_init(&H_CC2640R2F_I2Cparams);
    
    H_CC2640R2F_I2Cparams.bitRate = H_CC2640R2F_IIC_BaudRate;
    H_CC2640R2F_I2Cparams.transferCallbackFxn = NULL;//h_cc2640r2f_iic_callbackFxn;//NULL;
    H_CC2640R2F_I2Cparams.transferMode = I2C_MODE_BLOCKING;//I2C_MODE_CALLBACK;//I2C_MODE_BLOCKING;
    H_CC2640R2F_I2Cparams.custom = NULL;
    
    H_CC2640R2F_I2CHandle = I2C_open(0, \
                                        &H_CC2640R2F_I2Cparams);
    
    if(NULL == H_CC2640R2F_I2CHandle)
    {
        return (-1);
    }
    return 0;
}

/*
static signed char h_cc2640r2f_iic_readonly(unsigned char addr, \
                                                unsigned char *rdata, \
                                                  unsigned short length)
{
    //signed char rslt = 0;
    I2C_Transaction i2cTransaction;
    
    i2cTransaction.writeBuf = NULL;
    i2cTransaction.writeCount = 0;
    i2cTransaction.readBuf = rdata;
    i2cTransaction.readCount = length;
    i2cTransaction.slaveAddress = addr;
    i2cTransaction.arg = NULL;
    if(I2C_transfer(H_CC2640R2F_I2CHandle, &i2cTransaction))
    {
        return 0;
    }
    return (-1);
}

static signed char h_cc2640r2f_iic_writeonly(unsigned char addr, \
                                                unsigned char *wdata, \
                                                  unsigned short length)
{
    //signed char rslt = 0;
    I2C_Transaction i2cTransaction;
    
    i2cTransaction.writeBuf = wdata;
    i2cTransaction.writeCount = length;
    i2cTransaction.readBuf = NULL;
    i2cTransaction.readCount = 0;
    i2cTransaction.slaveAddress = addr;
    i2cTransaction.arg = NULL;
    if(I2C_transfer(H_CC2640R2F_I2CHandle, &i2cTransaction))
    {
        return 0;
    }
    return (-1);
}
*/

signed char H_CC2640R2F_IIC_ReadBuffer(unsigned char addr, \
                                        unsigned char reg, \
                                          unsigned char *rdata, \
                                            unsigned short length)
{
    I2C_Transaction i2cTransaction;

    i2cTransaction.writeBuf = &reg;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readBuf = rdata;
    i2cTransaction.readCount = length;
    i2cTransaction.slaveAddress = addr;
    i2cTransaction.arg = NULL;
    if(I2C_transfer(H_CC2640R2F_I2CHandle, &i2cTransaction))
    {
        ;
    }
    else
    {
        return (-1);
    }
    
    // i2cTransaction.writeBuf = &reg;
    // i2cTransaction.writeCount = 1;
    // i2cTransaction.readBuf = NULL;
    // i2cTransaction.readCount = 0;
    // i2cTransaction.slaveAddress = addr;
    // i2cTransaction.arg = NULL;
    // if(I2C_transfer(H_CC2640R2F_I2CHandle, &i2cTransaction))
    // {
    //     ;
    // }
    // else
    // {
    //     return (-1);
    // }
    
    // i2cTransaction.writeBuf = NULL;
    // i2cTransaction.writeCount = 0;
    // i2cTransaction.readBuf = rdata;
    // i2cTransaction.readCount = length;
    // i2cTransaction.slaveAddress = addr;
    // i2cTransaction.arg = NULL;
    // if(I2C_transfer(H_CC2640R2F_I2CHandle, &i2cTransaction))
    // {
    //     ;
    // }
    // else
    // {
    //     return (-2);
    // }
    
    return 0;
}

static signed char h_cc2640r2f_iic_memcpy(unsigned char *dest, \
                                            const unsigned char *src, \
                                              unsigned short length)
{
    if(dest == NULL || src == NULL)
    {
        return (-1);
    }
    while(length--)
    {
        *dest++ = *src++;
    }
    return 0;
}

signed char H_CC2640R2F_IIC_WriteBuffer(unsigned char addr, \
                                        unsigned char reg, \
                                          unsigned char *wdata, \
                                            unsigned short length)
{
    I2C_Transaction i2cTransaction;
    signed char rslt = 0;
    
    unsigned char *wdata_t = (unsigned char *)ICall_malloc(sizeof(unsigned char) * (length + 1));
    
    if(NULL == wdata_t)
    {
        return (-3);
    }
    wdata_t[0] = reg;
    
    h_cc2640r2f_iic_memcpy(&wdata_t[1], wdata, length);
    
    i2cTransaction.writeBuf = wdata_t;
    i2cTransaction.writeCount = length + 1;
    i2cTransaction.readBuf = NULL;
    i2cTransaction.readCount = 0;
    i2cTransaction.slaveAddress = addr;
    i2cTransaction.arg = NULL;
    if(I2C_transfer(H_CC2640R2F_I2CHandle, &i2cTransaction))
    {
        ;
    }
    else
    {
        rslt = -1;
    }
    
    ICall_free(wdata_t);
    wdata_t = NULL;
    
    return rslt;
}

/*
static void H_CC2640R2F_IIC_TestNew(void)
{
    unsigned char wdata[5] = {0x00};
    unsigned char rdata[5];
    
    I2C_Transaction i2cTransaction;
    
    
    i2cTransaction.writeBuf = wdata;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readBuf = NULL;
    i2cTransaction.readCount = 0;
    i2cTransaction.slaveAddress = 0x5A;
    i2cTransaction.arg = NULL;
    if(I2C_transfer(H_CC2640R2F_I2CHandle, &i2cTransaction))
    {
        H_CC2640R2F_UART_Debug("Set reg success\r\n");
    }
    else
    {
        H_CC2640R2F_UART_Debug("Set reg failed\r\n");
    }
    
    
    i2cTransaction.writeBuf = NULL;
    i2cTransaction.writeCount = 0;
    i2cTransaction.readBuf = rdata;
    i2cTransaction.readCount = 1;
    i2cTransaction.slaveAddress = 0x5A;
    i2cTransaction.arg = NULL;
    if(I2C_transfer(H_CC2640R2F_I2CHandle, &i2cTransaction))
    {
        H_CC2640R2F_UART_Debug("Read ChipID success\r\n");
    }
    else
    {
        H_CC2640R2F_UART_Debug("Read ChipID failed\r\n");
    }
    
    
}
*/

#include <stdio.h>
#define READDATA_LEN                    8
signed char H_CC2640R2F_IIC_Test(void)
{

   signed char rslt = 0;

   unsigned char data[READDATA_LEN];
//   unsigned char addr = 0x32;

   unsigned char i = 0U;

//    rslt = H_CC2640R2F_IIC_ReadBuffer(addr, 0x72, data, READDATA_LEN);
//
//    H_CC2640R2F_UART_Debug("IIC read rslt is %d\r\n", rslt);
//    for(i = 0U; i < READDATA_LEN; i++)
//    {
//        H_CC2640R2F_UART_Debug("0x%02x, ", data[i]);
//    }
//
//
//    H_CC2640R2F_UART_Debug("\r\n");

   for(i = 0U; i < 255; i++)
   {
       rslt = H_CC2640R2F_IIC_ReadBuffer(i, 0x00, data, READDATA_LEN);
       if(rslt)
       {
           printf(".");
       }
       else
       {
           printf("addr is %d\r\n", i);
       }
   }

   //H_CC2640R2F_IIC_TestNew();



   return 0;
}

