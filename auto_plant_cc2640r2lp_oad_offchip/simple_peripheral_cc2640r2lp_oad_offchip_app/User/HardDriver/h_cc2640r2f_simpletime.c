#include "h_cc2640r2f_simpletime.h"

#include <ti/sysbios/knl/Clock.h>

#include "h_cc2640r2f_define.h"



unsigned int H_CC2640R2F_SimpleTime_GetSystickms(void)
{
     return (Clock_getTicks() / 100);
}


void H_CC2640R2F_SimpleTime_Around_Delay(unsigned int dly)
{
    unsigned int i,j;
    for(i = 0;i < dly; i++)
    {
        for(j = 0; j < SIMPLE_AROUND_LOOP_PERIOD; j++)
        {
            asm(" nop ");
        }
    }
}

