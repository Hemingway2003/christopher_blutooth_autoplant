/*
 * h_cc2640r2f_rtc_innner.h
 *
 *  Created on: Sep 24, 2020
 *      Author: hemingway
 */

#ifndef USER_HARDDRIVER_H_CC2640R2F_RTC_INNNER_H_
#define USER_HARDDRIVER_H_CC2640R2F_RTC_INNNER_H_

extern signed char h_cc2640r2f_rtc_inner_test(void);
extern signed char h_cc2640r2f_rtc_inner_ch1_init(void);
extern signed char h_cc2640r2f_rtc_inner_ch1_setValueSec(unsigned int sec);

#endif /* USER_HARDDRIVER_H_CC2640R2F_RTC_INNNER_H_ */
