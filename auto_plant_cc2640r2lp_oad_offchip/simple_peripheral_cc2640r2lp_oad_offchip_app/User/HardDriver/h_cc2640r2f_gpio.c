/*
 * h_cc2640r2f_gpio.c
 *
 *  Created on: Sep 3, 2020
 *      Author: hemingway
 */
#include "h_cc2640r2f_gpio.h"

#include <stdio.h>
#include <string.h>
#include "h_cc2640r2f_define.h"
#include "s_auto_plant_msg.h"

static const PIN_Config H_CC2640R2F_GPIO_CFG[] =
{
    // Output part
    CC2640R2_LAUNCHXL_PIN_GLED | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,       /* LED initially off */
#if !LOCK_PWM_HARDWARE_MODE
    AUTO_PLANT_BUZZER_PWM_IO | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    AUTO_PLANT_A_PWM_IO | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    AUTO_PLANT_B_PWM_IO | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
#endif
    // AUTO_PLANT_A_PWM_IO | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    // AUTO_PLANT_B_PWM_IO | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    AUTO_PLANT_PERIPHERAL_ENABLE_IO | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    // Input part
    AUTO_PLANT_SD3077_INT_IO   | PIN_GPIO_OUTPUT_DIS  | PIN_INPUT_EN  |  PIN_PULLUP,
     
    PIN_TERMINATE
};

static PIN_Handle H_CC2640R2F_GPIO_Handle = NULL;
static PIN_State H_CC2640R2F_GPIO_State;

static void H_CC2640R2F_GPIO_InputCallback(PIN_Handle hPin, PIN_Id pin_Id)
{
   // printf("Key down\r\n");
   // S_LockControl_Ask2GPIOInput();
    S_AutoPlant_Ask_Handle_RTC();
}

signed char H_CC2640R2F_GPIO_Init(void)
{
    H_CC2640R2F_GPIO_Handle = PIN_open(&H_CC2640R2F_GPIO_State, \
                                                    H_CC2640R2F_GPIO_CFG);
#if !USE_INNER_RTC
   // For the input register
   PIN_registerIntCb(H_CC2640R2F_GPIO_Handle, \
                       H_CC2640R2F_GPIO_InputCallback);
   PIN_setConfig(H_CC2640R2F_GPIO_Handle, PIN_BM_IRQ, \
                 AUTO_PLANT_SD3077_INT_IO | PIN_IRQ_NEGEDGE);
#endif

    return 0;
}

/*
    Set the Output GPIO value.
*/
signed char H_CC2640R2F_GPIO_OutputSet(unsigned char PinID, unsigned int value)
{
    if(NULL == H_CC2640R2F_GPIO_Handle)
    {
        return (-1);
    }
    PIN_setOutputValue(H_CC2640R2F_GPIO_Handle, PinID, value);
    return 0;
}

// /*
//     Toogle the Output GPIO
// */
// signed char H_CC2640R2F_GPIO_OutputToggle(unsigned char PinID)
// {
//     if(NULL == H_CC2640R2F_GPIO_Handle)
//     {
//         return (-1);
//     }
//     PIN_setOutputValue(H_CC2640R2F_GPIO_Handle, PinID, \
//                         (PIN_getOutputValue(PinID) == 0)?1:0);

//     return 0;
// }



