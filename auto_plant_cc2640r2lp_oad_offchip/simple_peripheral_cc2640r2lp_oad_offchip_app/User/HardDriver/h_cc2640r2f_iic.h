#ifndef _H_CC2640R2F_IIC_H_
#define _H_CC2640R2F_IIC_H_

extern signed char H_CC2640R2F_IIC_Init(void);

extern 
signed char H_CC2640R2F_IIC_WriteBuffer(unsigned char addr, \
                                        unsigned char reg, \
                                          unsigned char *wdata, \
                                            unsigned short length);
extern signed char H_CC2640R2F_IIC_ReadBuffer(unsigned char addr, \
                                        unsigned char reg, \
                                          unsigned char *rdata, \
                                            unsigned short length);

extern signed char H_CC2640R2F_IIC_Test(void);

#endif
