#ifndef _H_CC2640R2F_SIMPLETIME_H_
#define _H_CC2640R2F_SIMPLETIME_H_


extern unsigned int H_CC2640R2F_SimpleTime_GetSystickms(void);


extern void H_CC2640R2F_SimpleTime_Around_Delay(unsigned int dly);

#endif
