/*
 * s_auto_plant.h
 *
 *  Created on: Sep 4, 2020
 *      Author: hemingway
 */

#ifndef USER_AUTOPLANT_S_AUTO_PLANT_H_
#define USER_AUTOPLANT_S_AUTO_PLANT_H_

extern signed char S_Auto_Plant_Init(void);
extern signed char S_Auto_Plant_Handle_UTC_Intr(void);
extern signed char S_Auto_Plant_Set_Start_Plan(void);

extern signed char S_Auto_Plant_Handle_Water_Plant(unsigned char plant_num);
extern void S_Auto_Plant_Handle_Water_Plant_Next(unsigned char plant_num);

extern void S_Auto_Plant_Enable_Peripheral_Power(void);
extern void S_Auto_Plant_Disable_Peripheral_Power(void);

#endif /* USER_AUTOPLANT_S_AUTO_PLANT_H_ */
