/*
 * s_auto_plant_msg.c
 *
 *  Created on: Sep 4, 2020
 *      Author: hemingway
 */
#include "s_auto_plant_msg.h"

#include <stdio.h>
#include <string.h>
#include "h_cc2640r2f_uart.h"
#include <icall_ble_api.h>

#include "h_cc2640r2f_adc.h"

#include "s_auto_plant.h"
#include "simple_peripheral_oad_offchip.h"

#include "h_cc2640r2f_pwm_simple.h"

#include "s_sd3077.h"

#include "s_auto_plant_wet.h"
#include "s_aht10.h"
#include "s_auto_plant_para.h"
#include <ti/sysbios/knl/Task.h>

#include "h_cc2640r2f_rtc_innner.h"

void S_Auto_Plant_Msg_Init(void)
{

}

#define S_AUTO_PLANT_MSG_GET_HEAD_1						'G'
#define S_AUTO_PLANT_MSG_GET_HEAD_2						'T'

#define S_AUTO_PLANT_MSG_SET_HEAD_1						'S'
#define S_AUTO_PLANT_MSG_SET_HEAD_2						'T'

#define S_AUTO_PLANT_MSG_BAT_HEAD_1					'B'
#define S_AUTO_PLANT_MSG_BAT_HEAD_2					'A'

#define S_AUTO_PLANT_MSG_ADC_HEAD_1					'A'
#define S_AUTO_PLANT_MSG_ADC_HEAD_2					'D'

#define S_AUTO_PLANT_MSG_ADC_VALUE_HEAD_1				'A'
#define S_AUTO_PLANT_MSG_ADC_VALUE_HEAD_2				'V'

#define S_AUTO_PLANT_MSG_MOTOR_HEAD_1					'M'
#define S_AUTO_PLANT_MSG_MOTOR_HEAD_2					'O'

#define S_AUTO_PLANT_MSG_UTC_HEAD_1						'T'
#define S_AUTO_PLANT_MSG_UTC_HEAD_2						'M'

#define S_AUTO_PLANT_MSG_POWER_THREAD_HEAD_1			'P'
#define S_AUTO_PLANT_MSG_POWER_THREAD_HEAD_2			'T'

#define S_AUTO_PLANT_MSG_NEXT_CHECK_TIME_HEAD_1			'N'
#define S_AUTO_PLANT_MSG_NEXT_CHECK_TIME_HEAD_2			'T'

#define S_AUTO_PLANT_MSG_SINGLE_WATER_TIME_HEAR_1		'S'
#define S_AUTO_PLANT_MSG_SINGLE_WATER_TIME_HEAR_2		'W'

#define S_AUTO_PLANT_MSG_DAY_TIME_HEAD_1				'D'
#define S_AUTO_PLANT_MSG_DAY_TIME_HEAD_2				'T'

#define S_AUTO_PLANT_MSG_CHECK_TIME_HEAD_1 				'C'
#define S_AUTO_PLANT_MSG_CHECK_TIME_HEAD_2 				'T'

#define S_AUTO_PLANT_MSG_CHECK_TIMES_FLAG_HEAD_1		'C'
#define S_AUTO_PLANT_MSG_CHECK_TIMES_FLAG_HEAD_2		'F'

#define S_AUTO_PLANT_MSG_HUMIDITY_HEAD_1				'H'
#define S_AUTO_PLANT_MSG_HUMIDITY_HEAD_2				'M'

#define S_AUTO_PLANT_MSG_TEMPERATURE_HEAD_1				'T'
#define S_AUTO_PLANT_MSG_TEMPERATURE_HEAD_2				'P'

#define S_AUTO_PLANT_MSG_WATER_NOW_HEAD_1				'W'
#define S_AUTO_PLANT_MSG_WATER_NOW_HEAD_2				'N'

#define S_AUTO_PLANT_MSG_WATER_MODE_HEAD_1              'M'
#define S_AUTO_PLANT_MSG_WATER_MODE_HEAD_2              'D'

#define S_AUTO_PLANT_MSG_SPLIT_CHAR						':'
														
#define S_AUTO_PLANT_MSG_REPORT_BAT_SIZE				2 + 1 + 4	//BA:$(int)
#define S_AUTO_PLANT_MSG_REPORT_ADC_SIZE				2 + 1 + 1 + 4 //AD$(adcnum):$(int)
#define S_AUTO_PLANT_MSG_REPORT_ADC_VALUE_SIZE			2 + 1 + 1 + 4
#define S_AUTO_PLANT_MSG_REPORT_MOTOR_SIZE				2 + 1 + 1 + 4
#define S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE		2 + 1 + 4
#define S_AUTO_PLANT_MSG_REPORT_DAY_TIME_SIZE			2 + 1 + 1 + 4 + 1
#define S_AUTO_PLANT_MSG_REPORT_SINGLE_WATER_TIME_SIZE	(2 + 1 + 1 + 4)


#define S_AUTO_PLANT_MSG_MAX_SEND_DATALENGTH			32

static const unsigned char S_AUTO_PLANT_MSG_UNKNOW_MSG[] = "Unknown-Msg";
static const unsigned char S_AUTO_PLANT_MSG_PARA_ERROR[] = "Parament-Error";


static unsigned char s_auto_plant_msg_data_point[S_AUTO_PLANT_MSG_MAX_SEND_DATALENGTH];
static volatile unsigned short s_auto_plant_msg_data_length = 0U;

// static unsigned char s_auto_plant_msg_motor_num

unsigned char *S_Auto_Plant_Get_DataPoint(void)
{
    return s_auto_plant_msg_data_point;
}

unsigned short S_Auto_Plant_Get_DataLength(void)
{
    if(NULL == s_auto_plant_msg_data_point)
    {
        return 0U;
    }
    return s_auto_plant_msg_data_length;
}

void S_Auto_Plant_Msg_Send(const unsigned char *data, unsigned short length)
{
    if(length > S_AUTO_PLANT_MSG_MAX_SEND_DATALENGTH)
    {
        s_auto_plant_msg_data_length = 0U;
    }
    else
    {
        memcpy(s_auto_plant_msg_data_point, data, length);
        s_auto_plant_msg_data_length = length;
    }

    SimplePeripheral_SingleNotify();
}


signed char S_Auto_Plant_Msg_Handle(unsigned char *data, unsigned short len)
{
	s_auto_plant_msg_t *pData = (s_auto_plant_msg_t*)ICall_malloc(sizeof(s_auto_plant_msg_t));

	if(NULL != pData)
	{
		pData->data = ICall_malloc(sizeof(unsigned char) * len);
		if(NULL != pData->data)
		{
			pData->len = len;
		
			memcpy(pData->data, data, len);
			pData->msg_type = s_auto_plant_msg_raw_data;

			SimplePeripheral_enqueueMsg(SBP_USER_EVT, 0,
                                    			(uint8_t *) pData);
		}
		
	}

	return 0;
}

signed char S_AutoPlant_UnknowMsg(void)
{
	S_Auto_Plant_Msg_Send(S_AUTO_PLANT_MSG_UNKNOW_MSG, sizeof(S_AUTO_PLANT_MSG_UNKNOW_MSG));
	return 0;
}

signed char S_AutoPlant_ParaError(void)
{
	S_Auto_Plant_Msg_Send(S_AUTO_PLANT_MSG_PARA_ERROR, sizeof(S_AUTO_PLANT_MSG_PARA_ERROR));
	return 0;
}


signed char S_AutoPlant_Ask_Start_WaterPlant(unsigned char plant_num)
{
	s_auto_plant_msg_t *pData = (s_auto_plant_msg_t*)ICall_malloc(sizeof(s_auto_plant_msg_t));
	if(NULL != pData)
	{
		pData->msg_type = s_auto_plant_msg_start_water_plant;
		pData->para = plant_num;
		return SimplePeripheral_enqueueMsg(SBP_USER_EVT, 0, (unsigned char *)pData);
	}
    return 0;
}

signed char S_AutoPlant_Peripherial_Ask_Power_On(void)
{
	s_auto_plant_msg_t *pData = (s_auto_plant_msg_t*)ICall_malloc(sizeof(s_auto_plant_msg_t));
	if(NULL != pData)
	{
		pData->msg_type = s_auto_plant_msg_peripherial_power_on;
		return SimplePeripheral_enqueueMsg(SBP_USER_EVT, 0, (unsigned char *)pData);
	}
    return 0;
}

signed char S_AutoPlant_Peripherial_Ask_Power_Off(void)
{
	s_auto_plant_msg_t *pData = (s_auto_plant_msg_t*)ICall_malloc(sizeof(s_auto_plant_msg_t));
	if(NULL != pData)
	{
		pData->msg_type = s_auto_plant_msg_peripherial_power_off;
		return SimplePeripheral_enqueueMsg(SBP_USER_EVT, 0, (unsigned char *)pData);
	}
    return 0;
}

/*
	Ask to handle the event
*/
signed char S_Auto_Plant_Msg_Ask_Set_Get_Handle(s_auto_plant_msg_type msg_type, unsigned char *msg_data, unsigned short msg_data_len, unsigned char para)
{
	s_auto_plant_msg_t *pData = (s_auto_plant_msg_t*)ICall_malloc(sizeof(s_auto_plant_msg_t));
	if(NULL != pData)
	{
		// Set the msg type
		pData->msg_type = msg_type;
		// Set the para
		pData->para = para;

		if(0 != msg_data_len)
		{
			pData->data = (unsigned char *)ICall_malloc(sizeof(unsigned char) * msg_data_len);
			if(NULL != pData->data)
			{
				memcpy(pData->data, msg_data, msg_data_len);
			}
		} else {
			pData->data = NULL;

		}
		pData->len = msg_data_len;

		return SimplePeripheral_enqueueMsg(SBP_USER_EVT, 0, (unsigned char *)pData);
		
	}

	return 0;
}

signed char S_AutoPlant_Ask_Handle_RTC(void)
{
	S_Auto_Plant_Handle_UTC_Intr();
	return 0;
}

signed char S_Auto_Plant_Msg_Handle_RealHandle(unsigned char *data, unsigned short len)
{
	s_auto_plant_msg_type msg_type = s_auto_plant_msg_uknown_evt;
	// signed int char_int_data = 0U;

	if(S_AUTO_PLANT_MSG_GET_HEAD_1 == data[0] && S_AUTO_PLANT_MSG_GET_HEAD_2 == data[1])
	{
		msg_type = s_auto_plant_msg_get_evt;
	}
	else if(S_AUTO_PLANT_MSG_SET_HEAD_1 == data[0] && S_AUTO_PLANT_MSG_SET_HEAD_2 == data[1])
	{
		msg_type = s_auto_plant_msg_set_evt;
	}

	if(msg_type != s_auto_plant_msg_uknown_evt)
	{
		if(s_auto_plant_msg_get_evt == msg_type)
		{
			// bat
			if(S_AUTO_PLANT_MSG_BAT_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_BAT_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_bat;
			}
			// ad
			else if(S_AUTO_PLANT_MSG_ADC_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_ADC_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_ad;
			}
			// ad value
			if(S_AUTO_PLANT_MSG_ADC_VALUE_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_ADC_VALUE_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_ad_value;
			}
			// motor
			else if(S_AUTO_PLANT_MSG_MOTOR_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_MOTOR_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_motor;
			}
			// single water timer
			else if(S_AUTO_PLANT_MSG_SINGLE_WATER_TIME_HEAR_1 == data[2] && S_AUTO_PLANT_MSG_SINGLE_WATER_TIME_HEAR_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_single_water_time;
			}
			// power thread
			else if(S_AUTO_PLANT_MSG_POWER_THREAD_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_POWER_THREAD_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_power_thread;
			}
			// next check time
			else if(S_AUTO_PLANT_MSG_NEXT_CHECK_TIME_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_NEXT_CHECK_TIME_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_next_check_time;
			}
			// check times flag
			else if(S_AUTO_PLANT_MSG_CHECK_TIMES_FLAG_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_CHECK_TIMES_FLAG_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_check_times_flag;
			}
			// day time
			else if(S_AUTO_PLANT_MSG_DAY_TIME_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_DAY_TIME_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_day_time;
			}
			// humidity
			else if(S_AUTO_PLANT_MSG_HUMIDITY_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_HUMIDITY_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_humidity;
			}
			// temperature
			else if(S_AUTO_PLANT_MSG_TEMPERATURE_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_TEMPERATURE_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_get_temperature;
			} else if(S_AUTO_PLANT_MSG_WATER_MODE_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_WATER_MODE_HEAD_2 == data[3]) {
			    // Mode
			    msg_type = s_auto_plant_msg_get_work_water_mode;
			} else if(S_AUTO_PLANT_MSG_UTC_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_UTC_HEAD_2 == data[3]) {
			    // utc
			    msg_type = s_auto_plant_msg_get_utc;
			}
		}
		else if(s_auto_plant_msg_set_evt == msg_type)
		{
			// utc
			if(S_AUTO_PLANT_MSG_UTC_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_UTC_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_set_utc;
			}
			// ad value
			if(S_AUTO_PLANT_MSG_ADC_VALUE_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_ADC_VALUE_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_set_ad_value;
			}
			
			// // ad
			// else if(S_AUTO_PLANT_MSG_ADC_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_ADC_HEAD_2 == data[3])
			// {
			// 	msg_type = s_auto_plant_msg_set_ad;
			// }
			// motor
			else if(S_AUTO_PLANT_MSG_MOTOR_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_MOTOR_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_set_motor;
			}
			// single water timer
			else if(S_AUTO_PLANT_MSG_SINGLE_WATER_TIME_HEAR_1 == data[2] && S_AUTO_PLANT_MSG_SINGLE_WATER_TIME_HEAR_2 == data[3])
			{
				msg_type = s_auto_plant_msg_set_single_water_time;
			}
			// power thread
			else if(S_AUTO_PLANT_MSG_POWER_THREAD_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_POWER_THREAD_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_set_power_thread;
			}
			// next check time
			else if(S_AUTO_PLANT_MSG_NEXT_CHECK_TIME_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_NEXT_CHECK_TIME_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_set_next_check_time;
			}
			// check times flag
			else if(S_AUTO_PLANT_MSG_CHECK_TIMES_FLAG_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_CHECK_TIMES_FLAG_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_set_check_times_flag;
			}
			// day time
			else if(S_AUTO_PLANT_MSG_DAY_TIME_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_DAY_TIME_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_set_day_time;
			}
			// water now
			else if(S_AUTO_PLANT_MSG_WATER_NOW_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_WATER_NOW_HEAD_2 == data[3])
			{
				msg_type = s_auto_plant_msg_water_now;
			} else if(S_AUTO_PLANT_MSG_WATER_MODE_HEAD_1 == data[2] && S_AUTO_PLANT_MSG_WATER_MODE_HEAD_2 == data[3]) {
                // Mode
                msg_type = s_auto_plant_msg_set_work_water_mode;
            }

		}

	}

	switch(msg_type)
	{
		case s_auto_plant_msg_get_bat:
		case s_auto_plant_msg_get_power_thread:
		case s_auto_plant_msg_get_next_check_time:
		case s_auto_plant_msg_get_check_times_flag:
		case s_auto_plant_msg_get_humidity:
		case s_auto_plant_msg_get_temperature:
		case s_auto_plant_msg_get_work_water_mode:
		case s_auto_plant_msg_get_utc:
			S_Auto_Plant_Msg_Ask_Set_Get_Handle(msg_type, NULL, 0, NULL);
			break;

		case s_auto_plant_msg_get_ad:
		case s_auto_plant_msg_get_ad_value:
		case s_auto_plant_msg_get_motor:
		case s_auto_plant_msg_get_day_time:
		case s_auto_plant_msg_get_single_water_time:
			if(len < 5)
			{
				S_AutoPlant_ParaError();
			}
			else
			{
				S_Auto_Plant_Msg_Ask_Set_Get_Handle(msg_type, NULL, 0, data[4]);
			}
			break;

		////////////////////////////////////////

		case s_auto_plant_msg_set_utc:
		case s_auto_plant_msg_set_power_thread:
		case s_auto_plant_msg_set_next_check_time:
		case s_auto_plant_msg_set_check_times_flag:
		case s_auto_plant_msg_set_work_water_mode:
			if(len < 9)
			{
				S_AutoPlant_ParaError();
			}
			else
			{
				S_Auto_Plant_Msg_Ask_Set_Get_Handle(msg_type, &data[5], 4, NULL);
			}
			break;

		case s_auto_plant_msg_set_ad:
		case s_auto_plant_msg_set_ad_value:
		case s_auto_plant_msg_set_motor:
		case s_auto_plant_msg_set_day_time:
		case s_auto_plant_msg_set_single_water_time:
			if(len < 10)
			{
				S_AutoPlant_ParaError();
			}
			else
			{
				S_Auto_Plant_Msg_Ask_Set_Get_Handle(msg_type, &data[6], 4, data[4]);
			}
			break;

		case s_auto_plant_msg_water_now:
			S_Auto_Plant_Msg_Ask_Set_Get_Handle(msg_type, NULL, 0, NULL);
			break;

		default:
			S_AutoPlant_UnknowMsg();
			break;
	}

	return 0;
}



signed char S_AutoPlant_GetBattery(void)
{
	unsigned int int_data = 0;
	unsigned char data[S_AUTO_PLANT_MSG_REPORT_BAT_SIZE] = \
	{S_AUTO_PLANT_MSG_BAT_HEAD_1, \
		S_AUTO_PLANT_MSG_BAT_HEAD_2, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	int_data = H_CC2640R2F_ADC_Read_MicroVolts_byNum(0);
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_BAT_SIZE - sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}

// signed char S_AutoPlant_SetUTC(unsigned char *ucdata)
// {
// 	unsigned int int_data = 0;
// 	memcpy(&int_data, ucdata, sizeof(int_data));
// 	// printf("the utc is %d\r\n", utc_int);
// 	S_SD3077_SetDate(utc_int);
// 	S_Auto_Plant_Msg_Send("OK", 2);
// 	return 0;
// }

signed char S_AutoPlant_GetADC(unsigned char para_num)
{
	unsigned int int_data = 0;

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_ADC_SIZE] = \
	{S_AUTO_PLANT_MSG_ADC_HEAD_1, \
		S_AUTO_PLANT_MSG_ADC_HEAD_2, \
		para_num, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	// printf("get ad\r\n");
	switch(para_num)
	{
		case '0':
		case '1':
			int_data = H_CC2640R2F_ADC_Read_MicroVolts_byNum(para_num - '0' + 1);
			break;
		default:
			S_AutoPlant_ParaError();
			return -1;
			// break;
	}
	
	// printf("adc %d\r\n", data);
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_ADC_SIZE - sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}


signed char S_AutoPlant_GetADCValue(unsigned char para_num)
{
	unsigned int int_data = 0;

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_ADC_VALUE_SIZE] = \
	{S_AUTO_PLANT_MSG_ADC_VALUE_HEAD_1, \
		S_AUTO_PLANT_MSG_ADC_VALUE_HEAD_2, \
		para_num, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	switch(para_num)
	{
		case '0':
		case '1':
			int_data = S_Auto_Plant_Para_Get_ADC_Value(&s_auto_plant_para, para_num - '0');
			break;
		default:
			S_AutoPlant_ParaError();
			return -1;
			// break;
	}
	
	// printf("adc %d\r\n", data);
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_ADC_VALUE_SIZE - sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}


signed char S_AutoPlant_GetMotor(unsigned char para_num)
{
	unsigned int int_data = 0;

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_MOTOR_SIZE] = \
	{S_AUTO_PLANT_MSG_MOTOR_HEAD_1, \
		S_AUTO_PLANT_MSG_MOTOR_HEAD_2, \
		para_num, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	switch(para_num)
	{
		case '0':
		case '1':
			int_data = S_Auto_Plant_Para_Get_Motor_Value(&s_auto_plant_para, para_num - '0');
			break;

		default:
			S_AutoPlant_ParaError();
			return -1;
			// break;
	}
	
	// printf("adc %d\r\n", data);
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_MOTOR_SIZE - \
		sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}

signed char S_AutoPlant_Get_SingleWaterTime(unsigned char para_num)
{
	unsigned int int_data = 0;

	unsigned char para_num_int = para_num - '0';
	// para_num -= '0';

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_SINGLE_WATER_TIME_SIZE] = \
	{S_AUTO_PLANT_MSG_SINGLE_WATER_TIME_HEAR_1, \
		S_AUTO_PLANT_MSG_SINGLE_WATER_TIME_HEAR_2, \
		para_num, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	int_data = S_Auto_Plant_Para_Get_Single_Water_Time_Uint_Seconds(&s_auto_plant_para, para_num_int);
	
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_SINGLE_WATER_TIME_SIZE - \
		sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}

signed char S_AutoPlant_Get_PowerThread(void)
{
	unsigned int int_data = 0;

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE] = \
	{S_AUTO_PLANT_MSG_POWER_THREAD_HEAD_1, \
		S_AUTO_PLANT_MSG_POWER_THREAD_HEAD_2, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	int_data = S_Auto_Plant_Para_Get_LowPower_Value(&s_auto_plant_para);
	
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE - \
		sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}

//

signed char S_AutoPlant_Get_NextCheckTime(void)
{
	unsigned int int_data = 0;

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE] = \
	{S_AUTO_PLANT_MSG_CHECK_TIME_HEAD_1, \
		S_AUTO_PLANT_MSG_CHECK_TIME_HEAD_2, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	int_data = S_Auto_Plant_Para_Get_Next_CheckSeconds(&s_auto_plant_para);
	
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE - \
		sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}

signed char S_AutoPlant_Get_CheckTimesFlag(void)
{
	unsigned int int_data = 0;

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE] = \
	{S_AUTO_PLANT_MSG_CHECK_TIMES_FLAG_HEAD_1, \
		S_AUTO_PLANT_MSG_CHECK_TIMES_FLAG_HEAD_2, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	int_data = S_Auto_Plant_Para_Get_MAX_CheckTimes(&s_auto_plant_para);
	
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE - \
		sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}

signed char S_AutoPlant_Get_DayTime(unsigned char para_num)
{
	unsigned int int_data = 0;

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_DAY_TIME_SIZE] = \
	{S_AUTO_PLANT_MSG_DAY_TIME_HEAD_1, \
		S_AUTO_PLANT_MSG_DAY_TIME_HEAD_2, \
		para_num, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	switch(para_num)
	{
		case '0':
		case '1':
		case '2':
			int_data = S_Auto_Plant_Para_Get_Water_Day_Time_Value(&s_auto_plant_para, para_num - '0');
			break;
		default:
			S_AutoPlant_ParaError();
			return -1;

	}

	printf("Get day time[%d] and the value is %d\r\n", para_num - '0', int_data);
	
	
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_DAY_TIME_SIZE - \
		sizeof(int_data) - 1], &int_data, sizeof(int_data));
	data[S_AUTO_PLANT_MSG_REPORT_DAY_TIME_SIZE - 1] = S_Auto_Plant_Para_Get_Water_Day_Time_Enable(&s_auto_plant_para, para_num - '0');

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}

signed char S_AutoPlant_Get_Humidity(void)
{
	signed int int_data, temp;

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE] = \
	{S_AUTO_PLANT_MSG_HUMIDITY_HEAD_1, \
		S_AUTO_PLANT_MSG_HUMIDITY_HEAD_2, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	S_AHT10_Start_Messure();
	Task_sleep(10000);
	S_AHT10_Get_Humidity_Temp(&int_data, &temp);
	// int_data = S_Auto_Plant_Para_Get_LowPower_Value(&s_auto_plant_para);
	
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE - \
		sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}

signed char S_AutoPlant_Get_Temperature(void)
{
	signed int hum, int_data;

	unsigned char data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE] = \
	{S_AUTO_PLANT_MSG_TEMPERATURE_HEAD_1, \
		S_AUTO_PLANT_MSG_TEMPERATURE_HEAD_2, \
		S_AUTO_PLANT_MSG_SPLIT_CHAR};

	S_AHT10_Start_Messure();
	Task_sleep(10000);
	S_AHT10_Get_Humidity_Temp(&hum, &int_data);
	// int_data = S_Auto_Plant_Para_Get_LowPower_Value(&s_auto_plant_para);
	
	memcpy(&data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE - \
		sizeof(int_data)], &int_data, sizeof(int_data));

	S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

	return 0;
}

signed char s_autoPlant_get_work_water_mode(void)
{
    unsigned int int_data = 0;

    unsigned char data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE] = \
    {S_AUTO_PLANT_MSG_WATER_MODE_HEAD_1, \
        S_AUTO_PLANT_MSG_WATER_MODE_HEAD_2, \
        S_AUTO_PLANT_MSG_SPLIT_CHAR};
    printf("Master get the water mode\r\n");
    int_data = S_Auto_Plant_Para_Get_Running_Mode(&s_auto_plant_para);

    memcpy(&data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE - \
        sizeof(int_data)], &int_data, sizeof(int_data));

    S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

    return 0;
}

signed char s_autoPlant_get_utc(void)
{
    unsigned int int_data = 0;

    unsigned char data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE] = \
    {S_AUTO_PLANT_MSG_UTC_HEAD_1, \
        S_AUTO_PLANT_MSG_UTC_HEAD_2, \
        S_AUTO_PLANT_MSG_SPLIT_CHAR};
    printf("get utc\r\n");
    // int_data = S_Auto_Plant_Para_Get_Running_Mode(&s_auto_plant_para);
    S_SD3077_GetDate(&int_data);

    memcpy(&data[S_AUTO_PLANT_MSG_REPORT_INT_SIZE_DATA_SIZE - \
        sizeof(int_data)], &int_data, sizeof(int_data));

    S_Auto_Plant_Msg_Send((unsigned char *)&data, sizeof(data));

    return 0;
}




signed char S_AutoPlant_SetADCValue(unsigned char para_num, unsigned char *para_value_char)
{
	unsigned int para_value_int = 0U;
	memcpy(&para_value_int, para_value_char, sizeof(para_value_int));


	unsigned char para_index_num = 0;

	para_index_num = para_num - '0';
	// if('A' == para_num)
	// {
	// 	motor_index_num = 1;
	// }
	// else if('B' == motor_num)
	// {
	// 	motor_index_num = 2;
	// }

	S_Auto_Plant_Para_Set_ADC_Value(&s_auto_plant_para, para_index_num, para_value_int);
	S_Auto_Plant_Para_Restore_To_Flash(&s_auto_plant_para);

	// H_CC2640R2F_PWM_SetDuty_ByIndex(motor_index_num, motor_value_int);
	// H_CC2640R2F_PWM_StartChannel_ByIndex(motor_index_num);
	// Task_sleep(100000);
	// H_CC2640R2F_PWM_StopChannel_ByIndex(motor_index_num);

	S_Auto_Plant_Msg_Send("OK", 2);

	return 0;
}

signed char S_AutoPlant_SetMotor(unsigned char motor_num, unsigned char *motor_value_char)
{
	unsigned int motor_value_int = 0U;
	memcpy(&motor_value_int, motor_value_char, sizeof(motor_value_int));


	unsigned char motor_index_num = motor_num - '0';

	// if('0' == motor_num)
	// {
	// 	motor_index_num = 1;
	// }
	// else if('1' == motor_num)
	// {
	// 	motor_index_num = 2;
	// }

	printf("Set the motor %d value to %d\r\n", motor_index_num, motor_value_int);

	S_Auto_Plant_Para_Set_Motor_Value(&s_auto_plant_para, motor_index_num, motor_value_int);
	S_Auto_Plant_Para_Restore_To_Flash(&s_auto_plant_para);

	H_CC2640R2F_PWM_SetDuty_ByIndex(motor_index_num + 1, motor_value_int);
	H_CC2640R2F_PWM_StartChannel_ByIndex(motor_index_num + 1);
	Task_sleep(100000);
	H_CC2640R2F_PWM_StopChannel_ByIndex(motor_index_num + 1);

	S_Auto_Plant_Msg_Send("OK", 2);

	S_AutoPlant_GetMotor(motor_num);

	return 0;
}

signed char S_AutoPlant_SetUTC(unsigned char *utc)
{
	unsigned int utc_int = 0;
	memcpy(&utc_int, utc, sizeof(utc_int));
	printf("the utc is %d\r\n", utc_int);
	S_SD3077_SetDate(utc_int);
	S_Auto_Plant_Msg_Send("OK", 2);
	s_autoPlant_get_utc();
	return 0;
}

signed char S_AutoPlant_Set_SingleTimeWater(unsigned char para_num, unsigned char *para_value_char)
{
	unsigned int para_value_int = 0;
	para_num -= '0';
	memcpy(&para_value_int, para_value_char, sizeof(para_value_int));
	printf("Single water time %d set to %d\r\n", para_num, para_value_int);
	S_Auto_Plant_Para_Set_Single_Water_Time_Uint_Seconds(&s_auto_plant_para, para_num, para_value_int);
	S_Auto_Plant_Para_Restore_To_Flash(&s_auto_plant_para);
	S_Auto_Plant_Msg_Send("OK", 2);
	S_AutoPlant_Get_SingleWaterTime(para_num + '0');
	return 0;
}

//
signed char S_AutoPlant_Set_PowerThread(unsigned char *para_value_char)
{
	unsigned int para_value_int = 0;
	memcpy(&para_value_int, para_value_char, sizeof(para_value_int));
	
	S_Auto_Plant_Para_Set_LowPower_Value(&s_auto_plant_para, para_value_int);
	S_Auto_Plant_Para_Restore_To_Flash(&s_auto_plant_para);
	S_Auto_Plant_Msg_Send("OK", 2);
	return 0;
}

signed char S_AutoPlant_Set_NextCheckTime(unsigned char *para_value_char)
{
	unsigned int para_value_int = 0;
	memcpy(&para_value_int, para_value_char, sizeof(para_value_int));
	
	S_Auto_Plant_Para_Set_Next_CheckSeconds(&s_auto_plant_para, para_value_int);
	S_Auto_Plant_Para_Restore_To_Flash(&s_auto_plant_para);
	S_Auto_Plant_Msg_Send("OK", 2);
	return 0;
}

signed char S_AutoPlant_Set_CheckTimesFlag(unsigned char *para_value_char)
{
	unsigned int para_value_int = 0;
	memcpy(&para_value_int, para_value_char, sizeof(para_value_int));
	
	S_Auto_Plant_Para_Set_MAX_CheckTimes(&s_auto_plant_para, para_value_int);
	S_Auto_Plant_Para_Restore_To_Flash(&s_auto_plant_para);
	S_Auto_Plant_Msg_Send("OK", 2);
	return 0;
}

signed char S_AutoPlant_Water_Now(void)
{
	S_Auto_Plant_Handle_UTC_Intr();
	S_Auto_Plant_Msg_Send("OK", 2);
	return 0;
}

signed char s_autoPlant_set_work_water_mode(unsigned char *para_value_char)
{
    unsigned int para_value_int = 0;
    memcpy(&para_value_int, para_value_char, sizeof(para_value_int));

    S_Auto_Plant_Para_Set_Running_Mode(&s_auto_plant_para, para_value_int);
    S_Auto_Plant_Para_Restore_To_Flash(&s_auto_plant_para);
    S_Auto_Plant_Msg_Send("OK", 2);
    s_autoPlant_get_work_water_mode();
    return 0;
}

signed char s_autoPlant_set_day_time(unsigned char para_num, unsigned char *para_value_char)
{
	unsigned int para_value_int = 0;
	unsigned char para_value_num_int = para_num - '0';
	
	memcpy(&para_value_int, para_value_char, sizeof(para_value_int));
	S_Auto_Plant_Para_Set_Water_Day_Time(&s_auto_plant_para, para_value_num_int, para_value_int);
	// S_Auto_Plant_Para_Set_Single_Water_Time_Uint_Seconds(&s_auto_plant_para, para_num, para_value_int);
	S_Auto_Plant_Para_Restore_To_Flash(&s_auto_plant_para);
	S_Auto_Plant_Msg_Send("OK", 2);
	S_AutoPlant_Get_DayTime(para_num);
	// Set it now
	S_Auto_Plant_Set_Start_Plan();
	return 0;
}


signed char S_AutoPlant_Peripherial_Power_On(void)
{
	S_Auto_Plant_Enable_Peripheral_Power();
	return 0;
}

signed char S_AutoPlant_Peripherial_Power_Off(void)
{
	S_Auto_Plant_Disable_Peripheral_Power();
	return 0;
}

void s_autoplant_processMsg(s_auto_plant_msg_t *msg)
{
	switch(msg->msg_type)
	{
		// case s_auto_plant_msg_raw_data:
		// 	printf("get ble data\r\n");
		// 	// S_Auto_Plant_Msg_Handle_RealHandle(msg->data, msg->len);
		// 	break;

		case s_auto_plant_msg_get_bat:
			S_AutoPlant_GetBattery();
			break;

		case s_auto_plant_msg_get_ad:
			S_AutoPlant_GetADC(msg->para);
			break;
		case  s_auto_plant_msg_get_ad_value:
			S_AutoPlant_GetADCValue(msg->para);
			break;

		case s_auto_plant_msg_get_motor:
			S_AutoPlant_GetMotor(msg->para);
			break;

		case s_auto_plant_msg_get_single_water_time:
			S_AutoPlant_Get_SingleWaterTime(msg->para);
			break;

		case s_auto_plant_msg_get_power_thread:
			S_AutoPlant_Get_PowerThread();
			break;	

			//
		case s_auto_plant_msg_get_next_check_time:
			S_AutoPlant_Get_NextCheckTime();
			break;

		case s_auto_plant_msg_get_check_times_flag:
			S_AutoPlant_Get_CheckTimesFlag();
			break;

			//
		case s_auto_plant_msg_get_day_time:
			S_AutoPlant_Get_DayTime(msg->para);
			break;	

		case s_auto_plant_msg_get_humidity:
			S_AutoPlant_Get_Humidity();
			break;	

		case s_auto_plant_msg_get_temperature:
			S_AutoPlant_Get_Temperature();
			break;	
		case s_auto_plant_msg_get_work_water_mode:
		    s_autoPlant_get_work_water_mode();
		    break;

	    case s_auto_plant_msg_get_utc:
			s_autoPlant_get_utc();
	    	break;


		////////////////////////////////////////////
		case s_auto_plant_msg_set_utc:
			S_AutoPlant_SetUTC(msg->data);
			break;

		case s_auto_plant_msg_set_ad:
			S_AutoPlant_SetADCValue(msg->para, msg->data);
			break;

		case s_auto_plant_msg_set_motor:
			S_AutoPlant_SetMotor(msg->para, msg->data);
			break;

		case s_auto_plant_msg_set_single_water_time:
			S_AutoPlant_Set_SingleTimeWater(msg->para, msg->data);
			break;

		case s_auto_plant_msg_set_power_thread:
			S_AutoPlant_Set_PowerThread(msg->data);
			break;

		case s_auto_plant_msg_set_next_check_time:
			S_AutoPlant_Set_NextCheckTime(msg->data);
			break;

		case s_auto_plant_msg_set_check_times_flag:
			S_AutoPlant_Set_CheckTimesFlag(msg->data);
			break;

		case s_auto_plant_msg_water_now:
			S_AutoPlant_Water_Now();
			break;

		case s_auto_plant_msg_set_work_water_mode:
		    s_autoPlant_set_work_water_mode(msg->data);
		    break;

	    case s_auto_plant_msg_set_day_time:
	    	s_autoPlant_set_day_time(msg->para, msg->data);
	    	break;

		////////////////////////////////////////////
		case s_auto_plant_msg_rtc_intr:
			S_Auto_Plant_Handle_UTC_Intr();
			break;

		case s_auto_plant_msg_start_water_plant:
			S_Auto_Plant_Handle_Water_Plant(msg->para);
			break;

		case s_auto_plant_msg_peripherial_power_on:
			S_AutoPlant_Peripherial_Power_On();
			break;

		case s_auto_plant_msg_peripherial_power_off:
			S_AutoPlant_Peripherial_Power_Off();
			break;

        ////////////////////////////////////////////
		case s_auto_plant_msg_sys_rtc_inner:
//		    printf("it\r\n");
		    H_CC2640R2F_UART_TxBuff("it\r\n", 4);
//		    h_cc2640r2f_rtc_inner_ch1_setValueSec(3600);

		    break;

		default:
			break;
	}

	if(msg->data != NULL) {
		ICall_free(msg->data);
		msg->data = NULL;
	}

}

void s_autoplant_inner_rtc_ch1_intr_ask(void)
{
    S_Auto_Plant_Msg_Ask_Set_Get_Handle(s_auto_plant_msg_rtc_intr, NULL, 0, 0);
}

void timer_hookCH1_Handle(void)
{
    s_autoplant_inner_rtc_ch1_intr_ask();
}


