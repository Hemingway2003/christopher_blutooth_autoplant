/*
 * s_auto_plant_para.h
 *
 *  Created on: Sep 11, 2020
 *      Author: hemingway
 */

#ifndef USER_AUTOPLANT_S_AUTO_PLANT_PARA_H_
#define USER_AUTOPLANT_S_AUTO_PLANT_PARA_H_

typedef enum
{
	s_auto_plant_para_running_notRight = 0,
	s_auto_plant_para_running_spec_water_time_only,
	s_auto_plant_para_running_with_adc_value
}s_auto_plant_para_running_mode;

typedef struct
{
	unsigned int water_day_time[3];
	unsigned char water_day_time_enable[3];
}s_auto_plant_para_water_day_time_t;

typedef struct
{
	s_auto_plant_para_running_mode running_mode;
	unsigned int single_water_time[2];  //uint: second
	unsigned int single_water_time_complement[2];
	unsigned int plant_adc_value[2];
	s_auto_plant_para_water_day_time_t water_day_times;
	unsigned int plant_motor_value[2];
	unsigned int plant_low_power_thread;
	unsigned int plant_water_max_check_times;
	unsigned int plant_water_next_check_seconds;
}s_auto_plant_para_t;

extern s_auto_plant_para_t s_auto_plant_para;

extern signed char S_Auto_Plant_Para_Init(s_auto_plant_para_t *plant_para);
extern signed char S_Auto_Plant_Para_Reset(s_auto_plant_para_t *plant_para);

extern signed char S_Auto_Plant_Para_Test(void);
extern signed char S_Auto_Plant_Para_Restore_To_Flash(s_auto_plant_para_t *plant_para);
extern signed char S_Auto_Plant_Para_Update_From_Flash(s_auto_plant_para_t *plant_para);
extern void S_Auto_Plant_Para_Show(s_auto_plant_para_t *plant_para);
extern signed char S_Auto_Plant_Para_Check(s_auto_plant_para_t *plant_para);

extern signed char S_Auto_Plant_Para_Set_Single_Water_Time_Uint_Seconds(s_auto_plant_para_t *plant_para, unsigned char index, unsigned int sec);
extern unsigned int S_Auto_Plant_Para_Get_Single_Water_Time_Uint_Seconds(s_auto_plant_para_t *plant_para, unsigned char index);

extern signed char S_Auto_Plant_Para_Set_Running_Mode(s_auto_plant_para_t *plant_para, s_auto_plant_para_running_mode mode);
extern s_auto_plant_para_running_mode S_Auto_Plant_Para_Get_Running_Mode(s_auto_plant_para_t *plant_para);

extern signed char S_Auto_Plant_Para_Set_Water_Day_Time(s_auto_plant_para_t *plant_para, unsigned char index, unsigned int value);
extern unsigned char S_Auto_Plant_Para_Get_Water_Day_Time_Enable(s_auto_plant_para_t *plant_para, unsigned char index);
extern unsigned int S_Auto_Plant_Para_Get_Water_Day_Time_Value(s_auto_plant_para_t *plant_para, unsigned char index);

extern signed char S_Auto_Plant_Para_Disable_Water_Day_Time(s_auto_plant_para_t *plant_para, unsigned char index);

extern signed char S_Auto_Plant_Para_Set_Motor_Value(s_auto_plant_para_t *plant_para, unsigned char index, unsigned int value);
extern unsigned int S_Auto_Plant_Para_Get_Motor_Value(s_auto_plant_para_t *plant_para, unsigned char index);

extern signed char S_Auto_Plant_Para_Set_ADC_Value(s_auto_plant_para_t *plant_para, unsigned char index, unsigned int value);
extern unsigned int S_Auto_Plant_Para_Get_ADC_Value(s_auto_plant_para_t *plant_para, unsigned char index);

extern signed char S_Auto_Plant_Para_Set_LowPower_Value(s_auto_plant_para_t *plant_para, unsigned int value);
extern unsigned int S_Auto_Plant_Para_Get_LowPower_Value(s_auto_plant_para_t *plant_para);

extern signed char S_Auto_Plant_Para_Set_MAX_CheckTimes(s_auto_plant_para_t *plant_para, unsigned int value);
extern unsigned int S_Auto_Plant_Para_Get_MAX_CheckTimes(s_auto_plant_para_t *plant_para);

extern signed char S_Auto_Plant_Para_Set_Next_CheckSeconds(s_auto_plant_para_t *plant_para, unsigned int value);
extern unsigned int S_Auto_Plant_Para_Get_Next_CheckSeconds(s_auto_plant_para_t *plant_para);

extern signed char S_Auto_Plant_Battery_Check(s_auto_plant_para_t *plant_para);

#endif /* USER_AUTOPLANT_S_AUTO_PLANT_PARA_H_ */
