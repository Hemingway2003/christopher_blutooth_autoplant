/*
 * s_auto_plant_msg.h
 *
 *  Created on: Sep 4, 2020
 *      Author: hemingway
 */

#ifndef USER_AUTOPLANT_S_AUTO_PLANT_MSG_H_
#define USER_AUTOPLANT_S_AUTO_PLANT_MSG_H_

typedef enum
{
	s_auto_plant_msg_raw_data = 0,
	s_auto_plant_msg_set_evt,
	s_auto_plant_msg_get_evt,
	s_auto_plant_msg_uknown_evt,
	s_auto_plant_msg_get_bat,
	s_auto_plant_msg_get_ad,
	s_auto_plant_msg_get_ad_value,
	s_auto_plant_msg_get_motor,
	s_auto_plant_msg_get_single_water_time,
	s_auto_plant_msg_get_power_thread,
	s_auto_plant_msg_get_next_check_time,
	s_auto_plant_msg_get_check_times_flag,
	s_auto_plant_msg_get_day_time,
	s_auto_plant_msg_get_humidity,
	s_auto_plant_msg_get_temperature,
	s_auto_plant_msg_get_work_water_mode,
	s_auto_plant_msg_get_utc,
	s_auto_plant_msg_set_ad,
	s_auto_plant_msg_set_ad_value,
	s_auto_plant_msg_set_motor,
	s_auto_plant_msg_set_single_water_time,
	s_auto_plant_msg_set_power_thread,
	s_auto_plant_msg_set_next_check_time,
	s_auto_plant_msg_set_check_times_flag,
	s_auto_plant_msg_set_day_time,
	s_auto_plant_msg_set_work_water_mode,
	s_auto_plant_msg_water_now,
	s_auto_plant_msg_rtc_intr,
	s_auto_plant_msg_set_utc,
	s_auto_plant_msg_peripherial_power_on,
	s_auto_plant_msg_peripherial_power_off,
	s_auto_plant_msg_start_water_plant,
	s_auto_plant_msg_sys_rtc_inner,
}s_auto_plant_msg_type;

typedef struct
{
	s_auto_plant_msg_type msg_type;
	unsigned short len;
	unsigned char *data;
	unsigned char para;
}s_auto_plant_msg_t;

extern void S_Auto_Plant_Msg_Init(void);

extern signed char S_Auto_Plant_Msg_Handle(unsigned char *data, unsigned short len);
extern signed char S_Auto_Plant_Msg_Handle_RealHandle(unsigned char *data, unsigned short len);

extern signed char S_AutoPlant_AskBattery(void);
extern void s_autoplant_processMsg(s_auto_plant_msg_t *msg);

extern unsigned char *S_Auto_Plant_Get_DataPoint(void);
extern unsigned short S_Auto_Plant_Get_DataLength(void);
extern void S_Auto_Plant_Msg_Send(const unsigned char *data, unsigned short length);

extern signed char S_AutoPlant_Ask_Handle_RTC(void);

extern signed char S_AutoPlant_Ask_Start_WaterPlant(unsigned char plant_num);

extern signed char S_AutoPlant_Peripherial_Ask_Power_On(void);
extern signed char S_AutoPlant_Peripherial_Ask_Power_Off(void);

extern signed char S_Auto_Plant_Msg_Ask_Set_Get_Handle(s_auto_plant_msg_type msg_type, unsigned char *msg_data, unsigned short msg_data_len, unsigned char para);

#endif /* USER_AUTOPLANT_S_AUTO_PLANT_MSG_H_ */
