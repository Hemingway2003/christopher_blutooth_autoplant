/*
 * s_auto_plant_motor.h
 *
 *  Created on: Sep 4, 2020
 *      Author: hemingway
 */

#ifndef USER_AUTOPLANT_S_AUTO_PLANT_MOTOR_H_
#define USER_AUTOPLANT_S_AUTO_PLANT_MOTOR_H_

extern void S_Auto_Plant_Motor_Init(void);

#endif /* USER_AUTOPLANT_S_AUTO_PLANT_MOTOR_H_ */
