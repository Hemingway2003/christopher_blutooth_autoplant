/*
 * s_user_sys.h
 *
 *  Created on: Sep 2, 2020
 *      Author: hemingway
 */

#ifndef USER_SOFTWARE_S_USER_SYS_H_
#define USER_SOFTWARE_S_USER_SYS_H_

extern signed char S_Sys_Init_In_Main(void);
extern signed char S_Sys_Init_Before_BLE_Init(void);


#endif /* USER_SOFTWARE_S_USER_SYS_H_ */
