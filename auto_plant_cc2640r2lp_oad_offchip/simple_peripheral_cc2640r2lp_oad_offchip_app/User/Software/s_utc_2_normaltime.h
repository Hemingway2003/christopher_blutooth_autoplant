#ifndef _S_UTC_2_NORMALTIME_H_
#define _S_UTC_2_NORMALTIME_H_

typedef struct s_normaltime_str
{
	unsigned short year;
	unsigned char month;
	unsigned char day;
	unsigned char hour;
	unsigned char min;
	unsigned char sec;
	unsigned char weeknum;	//0 for sunday
}s_normaltime_str, *p_s_normaltime_str;

//extern signed char S_NormalTime2_Hex_UTC(p_s_normaltime_str nmt, unsigned char *utchex);
//extern signed char S_Hex_UTC2NormalTime(unsigned char *utchex, \
					p_s_normaltime_str nmt);

// extern void s_normaltime_int2hex(unsigned int utc, unsigned char *utchex);
// extern void s_normaltime_hex2int(unsigned char *utchex, unsigned int *utc);

// extern void s_normaltime_hex2int_fromlittlemode(unsigned char *utchex, unsigned int *utc);

extern signed char S_NormalTime2UTC(p_s_normaltime_str nmt, unsigned int *utc);
extern signed char S_UTC2NormalTime(unsigned int utc, \
					p_s_normaltime_str nmt);

extern void S_UTC2NormalTime_Test(void);

#endif
