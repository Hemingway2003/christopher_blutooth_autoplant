/*
 * s_user_sys.c
 *
 *  Created on: Sep 2, 2020
 *      Author: hemingway
 */
#include "s_user_sys.h"

#include "h_cc2640r2f_uart.h"
#include "h_cc2640r2f_gpio.h"
#include "h_cc2640r2f_adc.h"
#include "s_lock_buzzer.h"
#include "h_cc2640r2f_iic.h"

#include "s_auto_plant.h"
#include "s_auto_plant_msg.h"
#include "s_auto_plant_motor.h"
#include "s_auto_plant_wet.h"
#include "s_aht10.h"
#include "s_nor_flash.h"
#include "s_auto_plant_para.h"

#include "h_cc2640r2f_pwm_simple.h"
#include "h_cc2640r2f_simpletime.h"

#include "s_sd3077.h"

#include <stdio.h>
#include <ti/sysbios/knl/Task.h>

#include "h_cc2640r2f_rtc_innner.h"


signed char S_Sys_Init_In_Main(void)
{
    signed char init_rslt = 0;



    init_rslt += H_CC2640R2F_UART_Init();

    init_rslt += H_CC2640R2F_ADC_Init();

    init_rslt += H_CC2640R2F_GPIO_Init();

    init_rslt += S_Nor_Flash_Init();

     init_rslt += H_CC2640R2F_PWM_Init_MaxNums(3, 1024);

    // H_CC2640R2F_PWM_SetDuty_ByIndex(1, 100);
    // H_CC2640R2F_PWM_StartChannel_ByIndex(1);

//    init_rslt += S_Lock_Buzzer_Init();
    // S_Lock_Buzzer_Success();

    init_rslt += H_CC2640R2F_IIC_Init();

    init_rslt += S_Auto_Plant_Init();

    S_Auto_Plant_Msg_Init();
    
//    S_Lock_Buzzer_Test();

    // S_Auto_Plant_Wet_Init();
    S_Auto_Plant_Motor_Init();


//    printf("UART init success\r\n");

//    flash_open();
//    flash_close();
    // S_AutoPlant_CreateTask();

    return 0;
}

signed char S_Sys_Init_Before_BLE_Init(void)
{

//    unsigned int ad1 = H_CC2640R2F_ADC_Read_MicroVolts_byNum(1);
//    while(1) {
//        ad1 = H_CC2640R2F_ADC_Read_MicroVolts_byNum(1);
//        printf("ad1 %d\r\n", ad1);
//        H_CC2640R2F_SimpleTime_Around_Delay(500);
//    }


    // s_normaltime_str nmt;
    // S_SD3077_GetDateStr(&nmt);
    // unsigned int start_time = 12 * 3600 + 55 * 60 + 0;
// //    H_CC2640R2F_IIC_Test();
//     // S_Lock_Buzzer_Init();
//     // S_Lock_Buzzer_Success();

    // H_CC2640R2F_PWM_Init_ByNum(1, 102400);
    // H_CC2640R2F_PWM_ChannelSet_Duty_Persent(10);
    // H_CC2640R2F_PWM_StartChannel();
    // printf("pwm init rslt %d\r\n", H_CC2640R2F_PWM_Init_MaxNums(1, 1024));
    // S_Lock_Buzzer_Success();
    

    // H_CC2640R2F_PWM_SetDuty_ByIndex(1, 100);
    // H_CC2640R2F_PWM_SetDuty_ByIndex(2, 100);

    // H_CC2640R2F_PWM_StartChannel_ByIndex(1);
    // H_CC2640R2F_PWM_StartChannel_ByIndex(2);

//     H_CC2640R2F_PWM_StartChannel_ByIndex(2);

    // S_SD3077_Test();

    // S_Lock_Buzzer_Success_2();
    // S_Lock_Buzzer_LowPower();
    // H_CC2640R2F_IIC_Test();
    S_AHT10_Init();
    printf("S_Sys_Init_Before_BLE_Init\r\n");
    

    // while(1)
    // {
    //     unsigned int hum, temp;
    //     S_AHT10_Start_Messure();
    //     // H_CC2640R2F_SimpleTime_Around_Delay(200);
    //     Task_sleep(10000);
    //     S_AHT10_Get_Humidity_Temp(&hum, &temp);
    //     printf("hum %d, temp %d\r\n", hum, temp);
    //     H_CC2640R2F_SimpleTime_Around_Delay(500);
    //     // Task_sleep(500000);
    // }

    S_Auto_Plant_Para_Init(&s_auto_plant_para);

    // S_Auto_Plant_Para_Show(&s_auto_plant_para);
    // printf("-------------------\r\n");

    // printf("--------------------------\r\n");

    // S_Auto_Plant_Para_Set_Water_Day_Time(&s_auto_plant_para, 0, start_time);

    // S_Auto_Plant_Para_Set_Water_Day_Time(&s_auto_plant_para, 2, start_time + 60);
    // // S_Auto_Plant_Para_Show(&s_auto_plant_para);
    // // printf("-------------------\r\n");

    // S_Auto_Plant_Para_Set_Water_Day_Time(&s_auto_plant_para, 2, start_time + 120);
    // S_Auto_Plant_Para_Show(&s_auto_plant_para);

    // printf("-------------------\r\n");

    // S_Auto_Plant_Para_Set_Water_Day_Time(&s_auto_plant_para, 2, 123);
    // S_Auto_Plant_Para_Show(&s_auto_plant_para);
    // printf("-------------------\r\n");

    // S_Auto_Plant_Para_Disable_Water_Day_Time(&s_auto_plant_para, 0);
    // S_Auto_Plant_Para_Show(&s_auto_plant_para);
    // printf("-------------------\r\n");

    // S_Auto_Plant_Para_Disable_Water_Day_Time(&s_auto_plant_para, 1);
    // S_Auto_Plant_Para_Show(&s_auto_plant_para);
    // printf("-------------------\r\n");

    // S_Auto_Plant_Para_Disable_Water_Day_Time(&s_auto_plant_para, 0);
    // S_Auto_Plant_Para_Show(&s_auto_plant_para);
    // printf("-------------------\r\n");

    

    S_Auto_Plant_Set_Start_Plan();

    // S_Nor_Flash_Test();

    // S_Auto_Plant_Para_Init(&s_auto_plant_para);

    // S_Auto_Plant_Para_Test();

//    h_cc2640r2f_rtc_inner_test();
#if USE_INNER_RTC
    h_cc2640r2f_rtc_inner_ch1_init();
#endif
    // h_cc2640r2f_rtc_inner_ch1_setValueSec(3600);
    return 0;
}
