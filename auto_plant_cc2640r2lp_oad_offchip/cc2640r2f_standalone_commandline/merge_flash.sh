#!/bin/bash
BIM_PATH=$(dirname "$PWD")/bim_oad_offchip_cc2640r2lp_app/FlashOnly
STATCK_PATH=$(dirname "$PWD")/simple_peripheral_cc2640r2lp_oad_offchip_stack/FlashROM
APP_PATH=$(dirname "$PWD")/simple_peripheral_cc2640r2lp_oad_offchip_app/FlashROM

TPWD=$PWD

cd $BIM_PATH
BIM_HEX=`ls *.hex`
cd $TPWD

cd $STATCK_PATH
STATCK_HEX=`ls *.hex`
cd $TPWD

cd $APP_PATH
APP_HEX=`ls *.hex`
cd $TPWD


python3 hexmerge.py -o oad_merge.hex -r "0000:1FFFF" --overlap=error "$APP_PATH/$APP_HEX":0100:1FFFF "$BIM_PATH/$BIM_HEX":0000:1F5FF "$STATCK_PATH/$STATCK_HEX":F000:1EFFF