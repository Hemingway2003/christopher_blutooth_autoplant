#!/bin/bash

oldstr=simple_peripheral_cc2640r2lp_oad_offchip_basic_simple_0.0.19.2
newstr=simple_peripheral_cc2640r2lp_oad_offchip_basic_simple_0.0.19.3

path=../*offchip_stack/FlashROM/
sed -i "s/$oldstr/$newstr/g" `grep $oldstr -rl $path`

path=../*offchip_app/FlashROM/
sed -i "s/$oldstr/$newstr/g" `grep $oldstr -rl $path`
