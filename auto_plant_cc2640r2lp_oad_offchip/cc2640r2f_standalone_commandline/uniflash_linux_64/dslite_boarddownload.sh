#!/bin/bash

# directory of script
script=$(readlink -f "$0")
scriptDir=$(dirname "$script")

if [ "$#" -eq 0 ]; then
	echo "No arg error"
elif [ "$#" -ge 4 ]; then
	echo "Too many args"
else
	if [ "$#" -eq 1 ]; then
		echo "1"
		hexfiles="$1"
	elif [ "$#" -eq 2 ]; then
		echo "2"
		hexfiles="$1 $2"
	elif [ "$#" -eq 3 ]; then
		echo "3"
		hexfiles="$1 $2 $3"
	elif [ "$#" -eq 4 ]; then
		echo "4"
		hexfiles="$1 $2 $3 $4"
	fi

	args=(\"$scriptDir\"/ccs_base/DebugServer/bin/DSLite flash -c user_files/configs/cc2640r2f.ccxml -l user_files/settings/generated.ufsettings -s VerifyAfterProgramLoad=\"No verification\" -e -f -v $hexfiles)

	eval ${args[@]}
fi

