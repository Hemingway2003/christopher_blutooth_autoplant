#/bin/bash

BIM_PATH=$(dirname "$PWD")/bim_oad_offchip_cc2640r2lp_app/FlashOnly
STATCK_PATH=$(dirname "$PWD")/simple_peripheral_cc2640r2lp_oad_offchip_stack/FlashROM
APP_PATH=$(dirname "$PWD")/simple_peripheral_cc2640r2lp_oad_offchip_app/FlashROM

TPWD=$PWD

cd $BIM_PATH
BIM_HEX=`ls *.hex`
cd $TPWD

cd $STATCK_PATH
STATCK_HEX=`ls *.hex`
cd $TPWD

cd $APP_PATH
APP_OUT=`ls *.out`
cd $TPWD

cd uniflash_linux_64
echo $PWD
./dslite_boarddownload.sh $BIM_PATH/$BIM_HEX $STATCK_PATH/$STATCK_HEX 
./dslite_boarddownload.sh $APP_PATH/$APP_OUT

# ./merge_flash.sh
# cd uniflash_linux_64
# ./dslite_boarddownload.sh ../oad_merge.hex